<?php

use MVC\Controller as Controller;
use MVC\Twig as Twig;
use Core\Model\Group as Group;

class EnginesController extends Controller{
    
    public function indexAction(){
        $this->view()->display();
    }
    
    public function getSetAction(){
        $render = array();
        $text = "";
        if( @$_POST ){
            $VAR = explode("\n", $_POST['VAR']);
            foreach( $VAR as $row ){
                $tmp = trim($row);
                $tmp = explode(" ", $row);
                $tmp = trim(end($tmp));
                if( substr($tmp, strlen($tmp) - 1, 1) == ";" ){
                    $tmp = substr($tmp, 0, strlen($tmp) - 1);
                }
                if( substr($tmp, 0, 1) == "\$" ){
                    $tmp = substr($tmp, 1);
                }
                $tmp2[] = trim($tmp);
            }
            $VAR = $tmp2;
            unset($tmp2);
            $ln = "\r\n";
            foreach( $VAR as $row ){
                if( substr($row, 0, 1) == '$' ){
                    $row = substr($row, 1);
                }
                $VAR2 = $row;
                $row = strtoupper(substr($row, 0, 1)).substr($row, 1);

                if( !$row ){
                    continue;
                }

                $text .= "$ln";
                $text .= "    public function set$row( \$$VAR2 ){{$ln}";
                $text .= "        \$this->$VAR2 = \$$VAR2;{$ln}";

                if( substr($row, strlen($row)-3) == 'Obj' ){
                    $text .= "        if( \$this->{$VAR2} ){{$ln}";
                    $text .= "            \$this->".substr($VAR2, 0, strlen($VAR2)-3)."ID = \$this->{$VAR2}->getID();{$ln}";
                    $text .= "        }{$ln}";
                }

                $text .= "        return \$this;{$ln}";
                $text .= "    }{$ln}{$ln}";

                $text .= "    public function get$row(){{$ln}";

                if( substr($row, strlen($row)-3) == 'Obj' ){
                    $text .= "        if( !\$this->{$VAR2} ){{$ln}";
                    $text .= "            \$this->{$VAR2} = :XXXXXXX::getByID(\$this->".substr($VAR2, 0, strlen($VAR2)-3)."ID);{$ln}";
                    $text .= "        }{$ln}";
                }

                $text .= "        return \$this->$VAR2;{$ln}";
                $text .= "    }{$ln}";
            }
        }
        $render['rows'] = $text;
        $this->view()->display($render);
    }

}