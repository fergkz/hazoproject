<?php

use MVC\Controller as Controller;
use MVC\Twig as Twig;
use Core\System\File as File;
use Core\System\DataBaseConnection as Connection;
use Core\System\Functions as Functions;

class DaoReferencesController extends Controller{
    
    private $reference;
    private $jSonPath;
    
    public function __construct(){
        if( @$_GET['file'] ){
            $filename = str_replace(array("\\", "/"), DS, Functions::decrypt($_GET['file']));
            $path = str_replace(array("\\", "/"), DS, PATH);
            if( stristr($filename, $path) ){
                $this->jSonPath = $filename;
            }else{
                $this->jSonPath = PATH.DS.ltrim($filename, "\\..\/");
            }
            $this->reference = file_exists($this->jSonPath) ? json_decode(file_get_contents($this->jSonPath), true) : "";
        }
    }

    public function indexAction(){
        $render["uls"] = $this->listAll();
        $Twig = new Twig();
        $Template = $Twig->loadTemplate("dao-references/index.html");
        echo $Template->render($render);
    }
    
    public function changeAction(){
        
        if( isset($_POST) && $_POST ){
            debug($_POST,0,"\$_POST");
        }
        
        $render['pathJson'] = $this->jSonPath;
        $render['reference'] = $this->reference;
        $Twig = new Twig();
        $Template = $Twig->loadTemplate("dao-references/change.html");
        echo $Template->render($render);
    }
    
    public function changeTableAction(){
        if( $_POST ){
            if( !$_POST['class-name'] ){
                _setError("hz_dao_cls_err");
            }
            if( !$_POST['table-name'] ){
                _setError("hz_dao_tbl_err");
            }
            if( !$_POST['description'] ){
                _setError("hz_dao_descr_err");
            }
            $this->reference['ClassName'] = $_POST['class-name'];
            $this->reference['Table'] = $_POST['table-name'];
            $this->reference['Label'] = $_POST['description'];
            if( !_getErrors() ){
                $content = json_encode($this->reference, JSON_PRETTY_PRINT);
                if( !file_put_contents($this->jSonPath, $content) ){
                    sleep(7);
                    if( !file_put_contents($this->jSonPath, $content) ){
                        _setError("hz_dao_json_read");
                    }
                }
            }
        }else{
            $tmp = file_get_contents(str_replace(".class.json", ".class.php", $this->jSonPath));
            $tmp = @end(explode("namespace",trim($tmp)));
            $tmp = @explode(";", trim($tmp));
            $tmp = @$tmp[0]."\\".@end(explode(ds,str_replace(".class.json","",$this->jSonPath)));
            $render['className'] = $tmp;
        }
        $render['reference'] = $this->reference;
        $Twig = new Twig();
        $Template = $Twig->loadTemplate("dao-references/change-table.html");
        echo $Template->render($render);
    }
    
    public function changeColumnAction(){
        if( !empty($this->reference['Table']) ){
            $Class = $this->reference['ClassName'];
            $class_attributes = $Class::listAttributesName();
            
            if( $class_attributes ){
                foreach( $class_attributes as $row ){
                    if( @$this->reference['Column'] 
                            and array_key_exists($row, $this->reference['Column'])
                            and @$_GET['attribute'] !== $row){
                        continue;
                    }
                    $render['class_attributes'][] = $row;
                }
            }
            
            $Connection = new Connection();
            $render['table_info'] = $Connection->getTableInfoMysql($this->reference['Table'], true);
            
            if( @$this->reference['Column'] ){
                foreach( $this->reference['Column'] as $row ){
                    
                    if( array_key_exists($row['Name'], $render['table_info']) 
                            and @$this->reference['Column'][$_GET['attribute']]['Name'] != $row['Name']){
                        unset($render['table_info'][$row['Name']]);
                    }
                    
                }
            }
        }
        
        if( @$_GET['attribute'] ){
            $column = $this->reference['Column'][$_GET['attribute']];
        }else{
            $render['new_collumn'] = true;
            $column = array();
        }
        if( $_POST ){
            if( !$_POST['class-attribute'] ){
                _setError("hz_dao_atrb");
            }
            if( !$_POST['table-column'] ){
                _setError("hz_dao_col_err");
            }
            if( !$_POST['description'] ){
                _setError("hz_dao_descr_err");
            }
            if( !$_POST['column-type'] ){
                _setError("hz_dao_type_err");
            }
            $column['Attribute']            = @$_POST['class-attribute'];
            $column['Name']                 = @$_POST['table-column'];
            $column['Label']                = @$_POST['description'];
            $column['Null']                 = @(int)_coalesce($_POST['column-null'],0);
            $column['Length']               = @$_POST['column-length'];
            $column['Type']                 = @$_POST['column-type'];
            $column['Primary']              = @(int)_coalesce($_POST['column-primary'],0);
            $column['Unique']               = @(int)_coalesce($_POST['column-unique'],0);
            $column['AutoIncrement']        = @(int)_coalesce($_POST['column-auto'],0);
            $column['Default']              = @$_POST['column-default'];
            $column['Reference']['Table']   = @$_POST['column-reference-table'];
            $column['Reference']['Column']  = @$_POST['column-reference-column'];
            
            if( !_getErrors() ){
                if( @$_GET['attribute'] ){
                    if( $column['Attribute'] !== $_GET['attribute'] ){
                        unset($this->reference['Column'][$_GET['attribute']]);
                    }
                }
                $this->reference['Column'][$column['Attribute']] = $column;
                $content = json_encode($this->reference, JSON_PRETTY_PRINT);
                if( !file_put_contents($this->jSonPath, $content) ){
                    sleep(7);
                    if( !file_put_contents($this->jSonPath, $content) ){
                        _setError("hz_dao_json_read");
                    }
                }
            }
        }
        $render['reference'] = $this->reference;
        
        $render['column'] = $column;
        $this->view()->display($render);
    }

    private function listAll(){
        $list = array();
        foreach( $GLOBALS['config']['namespaces'] as $namespace => $directory ){
            $list[$namespace] = $this->listClassByDirectory(PATH.DS.$directory);
            
        }
        return $this->mountUl($list);
    }

    private function listClassByDirectory($directory){
        $list = array();
        $File = new File($directory, true);
        $files = $File->getFiles(".class.php",1);
        $folders = $File->getFolders();
        if( $folders ){
            foreach( $folders as $folder ){
                $list[$folder] = $this->listClassByDirectory($directory.$folder."/");
            }
        }
        if( $files ){
            foreach( $files as $file ){
                if( !stristr(file_get_contents($file), "MVC\\Model") ){
                    continue;
                }
                $list[$file] = $file;
            }
        }
        return count($list) > 0 ? $list : false;
    }

    private function mountUl($list){
        if( !is_array($list) ){ return ""; }
        $html  = "<ul>";
        foreach( $list as $ind => $row ){
            $html .= "<li>";
            if( is_array($row) ){
                $html .= $ind."".$this->mountUl($row);
            }elseif( !stristr($ind, ".class.php") ){
                $html .= $ind;
            }else{
                $descr = @end(explode(ds,$ind));
                $json = str_replace(".class.php", ".class.json", $ind);
                if( file_exists($json) ){
                    $descr .= "&nbsp; <img src='".url."/core/pages/template/cog.png' style='height:10px;padding:0;margin:0;'/>";
                }else{
                    $descr .= "&nbsp; <img src='".url."/core/pages/template/add.png' style='height:10px;padding:0;margin:0;'/>";
                }
                $url = Functions::crypt(str_replace(PATH, "", $json));
                $html .= "<a href='".url."/s/dao-references/change?file={$url}'>{$descr}</a>";
            }
            $html .= "</li>";
        }
        $html .= "</ul>";
        return $html;
    }
    
}