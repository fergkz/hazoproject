<?php

namespace MVC;
use Core\System\Functions as Functions;

class View{
    
    private $Twig;
    private $Template = false;
    private $cacheFilename;
    private $cacheFilepath;

    public function __construct( $options = array() ){
        $this->Twig = new Twig( $options );
        return $this;
    }
    
    private function loadTemplate(){
        $filename = $GLOBALS['request']['module'].DS.$GLOBALS['request']['submodule'].".html";
        if( !$this->Template && file_exists(_getModePath($GLOBALS['request']['mode']).DS."View".DS.$filename ) ){
            $this->Template = $this->Twig->loadTemplate( $filename );
        }
    }

    public function setTemplate( $filename ){
        $this->Template = $this->Twig->loadTemplate( $filename );
        return $this;
    }
    
    public function declareFunction( $functionName ){
        $this->Twig->declareFunction($functionName);
        return $this;
    }

    public function display( $render = array() ){
        $this->loadTemplate();
        if( $this->Template ){
            $this->Template->display($render);
            return $this;
        }else{
            _includeError(404);
            exit;
        }
    }
    
    public function render( array $render = array() ){
        $this->loadTemplate();
        if( $this->Template ){
            return $this->Template->render($render);
        }else{
            _includeError(404);
            exit;
        }
    }
    
    public function cache( $seconds = 3600 ){
        if( !$this->cacheFilepath && isset($GLOBALS['config']['template']['cache_filepath']) ){
            $this->cacheFilepath = path.ds.trim($GLOBALS['config']['template']['cache_filepath'], "\\..\/").ds;
        }
        if( !$this->cacheFilename ){
           $this->cacheFilename = $this->cacheFilepath.Functions::crypt($_SERVER['REQUEST_URI'], "CACHE");
        }
        
        $file = glob($this->cacheFilename.'*');
        $file = @$file[0];
        $filetime = substr($file, strlen($this->cacheFilename));
        
        if( file_exists($file) && (time() - (int)$filetime) < $seconds ){
            $handle = fopen($file, "r");
            $content = fread($handle, filesize($file));
            fclose($handle);
            die($content);
        }else{
            register_shutdown_function(function( $this ){
                $this->burnCacheFile();
            }, null, true);
        }
        ob_start();
        return $this;
    }
    
    private function burnCacheFile(){
        foreach( glob($this->cacheFilename.'*') as $row ){
            unlink($row);
        }
        $content = ob_get_contents();
        ob_end_clean();
        $filename = $this->cacheFilename.time();
        file_put_contents($filename, $content);
        chmod($filename, 0777);
        die($content);
    }
    
    public function setCacheFilename( $filename ){
        $this->cacheFilename = $this->cacheFilepath.$filename;
        return $this;
    }
    
    public function setCacheFilepath( $filepath ){
        if( strtoupper(substr($filepath, 0, strlen(path))) != strtoupper(path) ){
            $this->cacheFilepath = path.ds.trim($filepath, "\\..\/").ds;
        }else{
            $this->cacheFilename = $filepath.ds;
        }
        return $this;
    }

}