<?php

namespace MVC;

use Core\System\DataBaseConnection as DataBaseConnection;

class Model{

    private $_parse;
    private $_pk;
    private $_action; # [I]nsert, [U]pdate, [D]elete

    public function __construct(){
        
    }
    
    public static function getByID($id){
        $classCalled = get_called_class();
        $Model = new $classCalled();
        if( property_exists($Model, 'ID') ){
            $Model->ID = $id;
            return $Model->load();
        }else{
            _setError("hz_model_atr_not_exists", array("{{class}}" => $classCalled));
            return false;
        }
    }
    
    public static function getInstance( $id = null ){
        $Obj = self::getByID($id);
        if( $Obj ){
            return $Obj;
        }else{
            $classCalled = get_called_class();
            return new $classCalled();
        }
    }
    
    public static function listAll(){
        $classCalled = get_called_class();
        $Model = new $classCalled();
        $Model->returnParse = true;
        $Model->load();
        foreach( $Model->returnParse['Column'] as $column ){
            if( $column['Primary'] == 1 ){
                $columnPK = $column;
                break;
            }
        }
        $sql = "select {$columnPK['Name']} from {$Model->returnParse['Table']}";
        $res = _query($sql);
        $data = array();
        if( $res ){
            foreach( $res as $row ){
                $data[] = $classCalled::getInstance($row[$columnPK['Name']]);
            }
        }
        return count($data) > 0 ? $data : false;
    }
    
    public static function listAttributesName(){
        $attrs = get_class_vars(get_called_class());
        $data = array();
        if( $attrs ){
            foreach( $attrs as $attr => $null ){
                if( !in_array($attr, array("_parse","_pk","_action")) and !strpos($attr, "Obj") ){
                    $data[] = $attr;
                }
            }
        }
        return $data;
    }

    private function _parse(){
        $filename = str_replace(".php",'.json',_getClassPath( get_class($this) ));
        
        if( file_exists($filename) ){
            $this->_parse = (Array)json_decode(file_get_contents($filename), true);
            if( @$this->returnParse ){
                $this->returnParse = $this->_parse;
            }
            return true;
        }else{
            $className = get_called_class();
            $tableName = @$className::dbTableName;
            if( $tableName ){
                $this->_parse['ClassName'] = $className;
                $this->_parse['Table'] = $tableName;
                $this->_parse['Label'] = $tableName;
                
                $attrs = self::listAttributesName();
                
                $Connection = new DataBaseConnection();
                $infosDb = $Connection->getTableInfoMysql($tableName, true);
                
                $infosClass = self::listAttributesName();
                
                if( $infosDb ){
                    foreach( $infosDb as $ind => $row ){
                        if( in_array($ind, $infosClass) ){
                            $this->_parse['Column'][$ind] = $row;
                        }
                    }
                }
                return true;
            }else{
                _setError("hz_model_file_not_found", array("{{class}}" => str_replace("\\",'/',get_class($this)) ));
                return false;
            }
        }
        _setError("hz_model_obj_not_valid");
        return false;
    }

    public function save( $action = null ){
        if( $action && !in_array($action, array( "I", "U", "D" )) ){
            _setError("hz_model_action_not_valid", array('{{action}}' => $action));
            return false;
        }
        $this->_action = $action;
        $GLOBALS['CONFIG']['DATABASE']['TRANSACTION']['IN'] = true;
        $DataBaseConnection = new DataBaseConnection();

        try{
            if( !$this->_parse ){
                $this->_parse();
            }
            $update = false;
            $this->_pk = array();
            $this->new = new \stdClass();
            $this->old = new \stdClass();

            ###################################################################
            ## Validacao e atribuicao das colunas                            ##
            if( $this->_parse['Column'] ){
                foreach( $this->_parse['Column'] as $atr => $det ){
                    if( !property_exists($this, $atr) ){
                        _setError("hz_model_no_alter", array("{{field}}" => $det['Label']));
                        return false;
                    }
                    $this->new->$atr = $this->$atr;
                    $validAtr[] = $atr;

                    if( $det['Primary'] && $det['Primary'] == true ){
                        $this->_pk['Atribute'] = $atr;
                        $this->_pk['Column'] = $det['Name'];
                        $this->_pk['Label'] = $det['Label'];
                        $this->_pk['AutoIncrement'] = $det['AutoIncrement'];
                    }

                    if( count($det) > 0 ){
                        $consultaCampos[$atr] = "{$det['Name']} as $atr";
                    }
                }
            }
            ## Validacao e atribuicao das colunas                            ##
            ###################################################################
            ###################################################################
            ## Validacao da chave primaria                                   ##
            if( !$this->_pk || count($this->_pk) == 0 ){
                _setError("hz_model_class_pk", array("{{class}}" => $this->_parse['ClassName']));
                return false;
            }

            if( !property_exists($this, $this->_pk['Atribute']) ){
                _setError("hz_model_rec_atr_val", array( '{{attribute}}' => $this->_pk['Atribute'], "{{class}}" => $this->_parse['ClassName'] ));
                return false;
            }

            ## Validacao da chave primaria                                   ##
            ###################################################################
            ###################################################################
            ## Consulta elementos da tabela                                  ##

            $get = $this->_pk['Atribute'];
            # Se tem valor na chave primaria
            if( $this->$get ){
                if( $consultaCampos && is_array($consultaCampos) && count($consultaCampos) > 0 ){
                    $tmp = implode(", ", $consultaCampos);
                }else{
                    _setError("hz_model_class_fields", array("{{class}}" => $this->_parse['ClassName']));
                    return false;
                }
                $sql = " select $tmp from {$this->_parse['Table']} where {$this->_pk['Column']} = :pk ";
                $res = $DataBaseConnection->query($sql, array( "pk" => $this->$get ));
                $contOld = 0;
                if( $res ){
                    foreach( $res[0] as $ind => $reg ){
                        if( in_array((String)$ind, $validAtr) ){
                            $this->old->$ind = $reg;
                            $contOld++;
                        }
                    }
                }
                
                if( (String)$this->_pk['AutoIncrement'] != "0" || $contOld > 0 ){
                    $update = true;
                }
            }
            

            ## Consulta elementos da tabela                                  ##
            ###################################################################
            ###################################################################
            ## Triggers e Gravação dos dados                                 ##

            $acao = $this->_action ? $this->_action : "I";
            $acao = $update && $this->_action != "D" ? "U" : $acao;
            $this->_action = $acao;

            # Se for exclusão e não existir PK
            $get = $this->_pk['Atribute'];
            if( in_array($this->_action, array( 'U', 'D' )) && !$this->$get ){
                _raise("hz_model_require_field", array( "{{field}}" => $this->_pk['Label'] ));
            }

            ###### Before
            $this->_condicaoSave = "B";
            $this->inserting = $acao == 'I' ? true : false;
            $this->deleting  = $acao == 'D' ? true : false;
            $this->updating  = $acao == 'U' ? true : false;

            if( $acao == "U" && method_exists($this, "triggerBeforeUpdate") ){
                $this->triggerBeforeUpdate();
            }
            if( $acao == "I" && method_exists($this, "triggerBeforeInsert") ){
                $this->triggerBeforeInsert();
            }
            if( $acao == "D" && method_exists($this, "triggerBeforeDelete") ){
                $this->triggerBeforeDelete();
            }
            if( in_array($acao, array( "I", "U" )) && method_exists($this, "triggerBeforeInsertUpdate") ){
                $this->triggerBeforeInsertUpdate();
            }
            if( in_array($acao, array( "I", "U", "D" )) && method_exists($this, "triggerBeforeDeleteInsertUpdate") ){
                $this->triggerBeforeDeleteInsertUpdate();
            }
            ###### Validate
            if( !$this->_validate() ){
                return false;
            }
            if( _getErrors() ){
                return false;
            }

            ###### Execute
            
            if( $acao == "I" ){
                foreach( (Array)$this->new as $ind => $reg ){
                    $tmpCol[] = $this->_parse['Column'][$ind]['Name'];
                    $tmpReg[] = ":$ind";
//                    $tmpBind[$ind] = !empty($reg) ? (String)$reg : $this->_parse['Column'][$ind]['Default'];
                    $tmpBind[$ind] = $this->valueValidate($reg, $this->_parse['Column'][$ind]['Default']);
                }
                $set = implode(", ", $tmpCol);
                $val = implode(", ", $tmpReg);
                $sql = "insert into {$this->_parse['Table']} ($set) values ($val) ";
                $res = $DataBaseConnection->execute($sql, $tmpBind);
                $attrPK = $this->_pk['Atribute'];
                if( $res ){
                    if( $this->new->$attrPK ){
                        $this->$attrPK = $this->new->$attrPK;
                    }else{
                        if( property_exists($this, $attrPK) ){
                            $tmpValuePk = $DataBaseConnection->lastId();
                            $this->$attrPK = $tmpValuePk;
                            $this->new->$attrPK = $this->new->$attrPK ? $this->new->$attrPK : $tmpValuePk;
                        }else{
                            _setError("hz_model_method_not_exst", array( "{{method}}" => $set, "{{class}}" => get_class($this) ));
                            return false;
                        }
                    }
                }else{
                    return false;
                }
//                if( $this->_parse['GravaLog'] == "S" ){
//                    if( !$this->_gravaLog("I", $this->new, $this->_parse, $this->old) ){
//                        return false;
//                    }
//                }
            }elseif( $acao == "U" ){
                $contTodos = $contIguais = 0;
                foreach( (array)$this->new as $ind => $reg ){
                    $contTodos++;
                    if( $ind != $this->_pk['Atribute'] ){
                        if( $this->new->$ind === $this->old->$ind ){
                            $contIguais++;
                            continue;
                        }
                        $tmpSet[] = "{$this->_parse['Column'][$ind]['Name']} = :$ind";
                    }
//                    $reg = $reg ? $reg : $this->_parse['Column'][$ind]['Default'];
                    
                    $tmpBind[$ind] = $this->valueValidate($reg, $this->_parse['Column'][$ind]['Default']);
//                    $tmpBind[$ind] = !empty($reg) ? (String)$reg : null;
                }
                if( ($contTodos-$contIguais) > 1 && $contTodos != $contIguais ){
                    $tmpSet = implode(", ", $tmpSet);
                    $sql = " update {$this->_parse['Table']} set $tmpSet where {$this->_pk['Column']} = :{$this->_pk['Atribute']} ";
                    $res = $DataBaseConnection->execute($sql, $tmpBind);
                    if( !$res ){
                        return false;
                    }
    //                if( $this->_parse['GravaLog'] == "S" ){
    //                    if( !$this->_gravaLog("U", $this->new, $this->_parse, $this->old) ){
    //                        return false;
    //                    }
    //                }
                }
            }elseif( $acao == "D" ){
                $sql = "delete from {$this->_parse['Table']} where {$this->_pk['Column']} = :{$this->_pk['Atribute']}";
                $tmpPkAttr = $this->_pk['Atribute'];
                $res = $DataBaseConnection->execute($sql, array( "{$this->_pk['Atribute']}" => $this->new->$tmpPkAttr ));
                if( !$res ){
                    return false;
                }
//                if( $this->_parse['GravaLog'] == "S" ){
//                    if( !$this->_gravaLog("D", $this->new, $this->_parse, $this->old) ){
//                        return false;
//                    }
//                }
            }
            ###### After
            $this->_condicaoSave = "A";
            
            if( $acao == "U" && method_exists($this, "triggerAfterUpdate") ){
                $this->triggerAfterUpdate();
            }
            if( $acao == "I" && method_exists($this, "triggerAfterInsert") ){
                $this->triggerAfterInsert();
            }
            if( $acao == "D" && method_exists($this, "triggerAfterDelete") ){
                $this->triggerAfterDelete();
            }
            if( in_array($acao, array( "I", "U" )) && method_exists($this, "triggerAfterInsertUpdate") ){
                $this->triggerAfterInsertUpdate();
            }
            if( in_array($acao, array( "I", "U", "D" )) && method_exists($this, "triggerAfterDeleteInsertUpdate") ){
                $this->triggerAfterDeleteInsertUpdate();
            }

            ###### Validate
            if( !$this->_validate() ){
                return false;
            }

            if( _getErrors() ){
                return false;
            }

            ## Triggers e Gravação dos dados                                 ##
            ###################################################################

            if( @$DataBaseConnection->erroMessage || _getErrors() ){
                $DataBaseConnection->rollback();
                _raise($DataBaseConnection->erroMessage);
                return false;
            }else{
                $this->load();
                $GLOBALS['CONFIG']['DATABASE']['TRANSACTION']['IN'] = false;
                return $this;
            }
        }catch( \Exception $e ){
            $DataBaseConnection->rollback();
            _setError($e->getMessage());
            return false;
        }
    }
    
    private function valueValidate( $dataReceive, $replace = null ){
        if( !$dataReceive && $dataReceive !== 0 && ($replace || $replace === 0 || $replace === "0") ){
            return $replace;
        }else{
            return $dataReceive;
        }
    }

    public function load(){
        $DataBaseConnection = new DataBaseConnection();
        $this->_parse();
        if( $this->_parse['Column'] ){
            foreach( $this->_parse['Column'] as $atr => $col ){
                $select[] = "{$col['Name']} as $atr";
                if( $col['Primary'] ){
                    $pkAtr = $atr;
                    $pkCol = $col["Name"];
                }
            }
        }else{
            return false;
        }
        
        if( !$this->$pkAtr ){
            return false;
        }
        $bind[$pkCol] = $this->$pkAtr;
        if( $select ){
            $select = implode(", ", $select);
        }

        if( method_exists($this, "triggerBeforeLoad") ){
            $this->triggerBeforeLoad();
        }

        $sql = "select $select from {$this->_parse['Table']} where {$pkCol} = :$pkCol";
        $res = $DataBaseConnection->query($sql, $bind);
        if( $res ){
            foreach( $this->_parse['Column'] as $atr => $col ){
                $this->$atr = $res[0][$atr];
            }

            if( method_exists($this, "triggerAfterLoad") ){
                $this->triggerAfterLoad();
            }
            $this->_parse = null;
            return $this;
        }
        return false;
    }

    private function _validate(){
        $DataBaseConnection = new DataBaseConnection();
        $parse = $this->_parse;

        if( !empty($parse['Column']) ){
            foreach( $parse['Column'] as $atr => $data ){
                if( !property_exists($this, $atr) ){
                    return _raise("hz_model_atrr_not_found", array( "{{attribute}}" => $atr, "{{class}}" => $parse['ClassName'] ));
                }

                if( in_array($this->_action, array( "U", "I" )) ){
                    if( !$data['Null'] && empty($data['Default']) && in_array($data['Default'], array(null, "")) && $this->new->$atr == null && (!$data['Primary'] || ($data['Primary'] && !$data['AutoIncrement']) ) ){
                        return _raise("hz_model_require_field", array( "{{field}}" => $data['Label'] ));
                    }
                    # Se for chave estrangeira
                    if( $data['Reference'] && is_array($data['Reference']) && count($data['Reference']) < 1 && $this->_condicaoSave == "B" ){
                        if( !$data['Null'] && $this->new->$atr ){
                            $sql = "select {$data['Reference']['Label']} as descr from {$data['Reference']['Table']} where {$data['Reference']['Column']} = :fk";
                            $res = $DataBaseConnection->query($sql, array( "fk" => $this->new->$atr ));
                            if( !$res[0]['descr'] ){
                                return _raise("hz_model_value_not_found", array( "{{value}}" => $this->new->$atr, "{{table}}" => $data['Reference']['Table'] ));
                            }
                        }
                    }
                    # Se o campo for maior que o lenght da tabela
                    if( $data['Length'] && strlen($this->new->$atr) > $data['Length'] ){
                        _raise("hz_model_more_length", array( "{{length}}" => $data['Length'], "{{field}}" => $data['Label'] ));
                    }
                    # Validacao de chave unica
                    if( $data['Unique'] == "1" && $this->_action == "I" && $this->_condicaoSave == "B" ){
                        $sql = "select count(*) as cont from {$this->_parse['Table']} 
                                   where upper({$data['Name']}) like upper(:value)";
                        $res = $DataBaseConnection->query($sql, array( "value" => $this->new->$atr ));
                        if( $res[0]['cont'] > 0 ){
                            _raise("hz_model_unique_exists", array( "{{value}}" => $this->new->$atr, "{{field}}" => $data['Label'] ) );
                        }
                    }
                }
            }
        }
        return true;
    }

    public static function searchBySql($sqlQuery=null){
        if( !$sqlQuery ){
            return false;
        }
        $bind = array();
        $sqlQuery = str_replace(array('::','?',':term'), ":findterm", $sqlQuery);
        @$bind['findterm'] = "%{$_GET['term']}%";
        if( @$_GET['id'] ){
            $sql = " select hzsubalias.id, hzsubalias.label from ({$sqlQuery}) hzsubalias where id = :id";
            $bind['id'] = $_GET['id'];
        }else{
            $sql = " select hzsubalias.id, hzsubalias.label from ({$sqlQuery}) hzsubalias ";
        }
        if( @$_GET['limit'] ){
            $tmp = explode(",",trim($_GET['limit']));
            @$limit['start'] = trim($tmp[0]);
            @$limit['end']   = trim($tmp[1]);
            @$sql .= " limit ".$limit['start'].", ".$limit['end'];
        }
        @$res = @_query($sql, $bind);
        
        $a = array();
        if( @$res ){
            foreach( $res as $row ){
                @$a[] = array('id' => $row['id'], 'label' => $row['label']);
            }
        }
        return json_encode($a);
    }

}