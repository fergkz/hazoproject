<?php

define("DS", DIRECTORY_SEPARATOR);
define("ds",DS);
define("dirname_client_config", dirname(dirname(__DIR__)));

set_include_path( get_include_path().PATH_SEPARATOR.dirname_client_config );

include_once(__DIR__.DS."hazo_load_config_ini.php");
include_once(__DIR__.DS."hazo_load_config_get.php");
include_once(__DIR__.DS."hazo_autoload.php");
include_once(__DIR__.DS."hazo_functions.php");

register_shutdown_function("_shutdownExecuteCommit");

\MVC\Session::start();

include_once(__DIR__.DS."hazo_redirect.php");