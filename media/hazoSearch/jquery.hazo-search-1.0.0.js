jQuery.fn.hazoSearch = function(jsonParam){
    return this.each(function(){
                
        if( jsonParam && jsonParam.start !== undefined && jsonParam.end !== undefined ){
            limit = Number(jsonParam.end) - Number(jsonParam.start);
            limitParam = jsonParam.start+','+jsonParam.end;
        }else if( jsonParam && jsonParam.end !== undefined ){
            limitParam = '0,'+jsonParam.end;
            limit = Number(jsonParam.end);
        }else{
            limit = -1;
        }
        
        if( $(this).attr('data-url') !== undefined && $(this).attr('data-url') ){
            $(this).data("url", $(this).attr('data-url'));
        }else if( jsonParam && jsonParam.url !== undefined && jsonParam.url ){
            $(this).data("url", jsonParam.url);
        }else{
            return;
        }
        if( limit >= 0 ){
            $(this).data('url', $(this).data('url') + "?limit=" + limitParam );
        }
        
        if( $(this).val() !== undefined && $(this).val() ){
            $(this).data("id-val", $(this).val());
        }else if( jsonParam && jsonParam.value !== undefined && jsonParam.value ){
            $(this).data("id-val",jsonParam.value);
        }else{
            $(this).data("id-val","");
        }
        
        $(this).autocomplete({
            minLength: 0,
            source: $(this).data("url"),
            autoFocus: false,
            scroll: true,
            create: function(){
                var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                text = '';
                for( var i=0; i < 40; i++ ){
                    text += possible.charAt(Math.floor(Math.random() * possible.length));
                }
                    
                $hid = $("<input/>").attr("type","hidden")
                    .attr("name",$(this).attr('name'))
                    .attr("id", text )
                    .val($(this).val());
                
                $(this).data("real", $hid );
                $(this).after($hid);
                                
                $(this).attr('name','')
                .attr('id-search-reference', $(this).data("real").attr('id'))
                .after($(this).data("real"));
                
                if( $(this).data("id-val") ){
                    $(this).addClass("hazo-search-load");
                    thisElem = $(this);
                    jQuery.ajax({
                        url: thisElem.data("url"),
                        data: { "id": thisElem.data("id-val") },
                        dataType: 'json',
                        success: function(data) {
                            thisElem.data("real").val(data[0].id);
                            thisElem.val(data[0].label);
                        },
                        async: false
                    });   
                }
                $(this).removeClass("hazo-search-load").addClass("hazo-search");

            },
            search: function( event, ui ){
                $(this).removeClass("hazo-search").addClass("hazo-search-load");
            },
            open: function( event, ui ){
                $(this).removeClass("hazo-search-load").addClass("hazo-search");
            },
            select: function( event, ui ) {
                $(this).data("real").val('');
                $(this).val('');
                if( ui.item.id ){
                    $(this).data("real").val(ui.item.id);
                    $(this).val(ui.item.label);
                }
            },
            response: function( event, ui ) {
                if( limit >= 0 && ui.content.length >= limit ){
                    ui.content.push(
                    {
                        id:null, 
                        label:"........... Para mais resultados, refine a pesquisa"
                    }
                    );
                }
                if( ui.content.length == 0 ){
                    ui.content.push(
                    {
                        id:null, 
                        label:"Nenhum resultado encontrado"
                    }
                    );
                }
            }
        }).click(function(){
            $(this).autocomplete("search", "%");
        }).blur( function(event) {
            $(this).removeClass("hazo-search-load").addClass("hazo-search");
        }).change( function(event) {
            $(this).attr("data-before-value", "");
            $(this).removeClass("hazo-search-load").addClass("hazo-search");
                
            var autocomplete = $(this).data("autocomplete");
            //if there was a match trigger the select event on that match
            if( autocomplete === undefined || !autocomplete.selectedItem ){
                $(this).val('');
                $(this).data("real").val('');
            }
        });
        return $(this);
    });
};