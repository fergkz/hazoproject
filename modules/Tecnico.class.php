<?php

namespace Controller;

use Model\Usuario as Usuario;

class Tecnico extends \MVC\Controller{

    public function __construct(){
        
        $Usuario = Usuario::online();
        
        if( !$Usuario ){
            $this->redirect(url);
        }elseif( !in_array($Usuario->getTipoUsuario(), array('T','G')) ){
            _includeError(404);
            exit;
        }
        
    }
    
    public function json( $array = array() ){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        if( !$array || !is_array($array) ){
            $array = array();
        }
        echo json_encode($array);
        exit;
    }

}