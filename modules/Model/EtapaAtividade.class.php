<?php

namespace Model;

class EtapaAtividade extends \MVC\Model{
    
    protected $ID;
    protected $sequencia;
    protected $titulo;
    protected $descricao;
    protected $etapaID;
    protected $etapaObj;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setSequencia( $sequencia ){
        $this->sequencia = $sequencia;
        return $this;
    }

    public function getSequencia(){
        return $this->sequencia;
    }

    public function setTitulo( $titulo ){
        $this->titulo = $titulo;
        return $this;
    }

    public function getTitulo(){
        return $this->titulo;
    }

    public function setDescricao( $descricao ){
        $this->descricao = $descricao;
        return $this;
    }

    public function getDescricao(){
        return $this->descricao;
    }

    public function setEtapaID( $etapaID ){
        $this->etapaID = $etapaID;
        return $this;
    }

    public function getEtapaID(){
        return $this->etapaID;
    }

    public function setEtapaObj( $etapaObj ){
        $this->etapaObj = $etapaObj;
        if( $this->etapaObj ){
            $this->etapaID = $this->etapaObj->getID();
        }
        return $this;
    }

    public function getEtapaObj(){
        if( !$this->etapaObj ){
            $this->etapaObj = Etapa::getByID($this->etapaID);
        }
        return $this->etapaObj;
    }
    
    protected function triggerBeforeInsert(){
        $sql = "select coalesce(max(sequencia), 0) as sequencia
                  from cronograma_atividade where etapa_id = :etapa_id";
        $bind['etapa_id'] = $this->new->etapaID;
        $res = _query($sql, $bind);
        $this->new->sequencia = empty($res[0]['sequencia']) ? 1 : $res[0]['sequencia']+1;
    }
    
    public static function lista( $etapa = null ){
        
        $bind = array();
        $where = array();
        $dados = array();
        
        if( $etapa ){
            $bind['etapa_id'] = is_object($etapa) ? $etapa->getID() : $etapa;
            $where[] = ' etapa_id = :etapa_id ';
        }
        
        $sql = "select id from cronograma_atividade";
        if( $where && count($where) > 0 ){
            $sql .= " where ".implode(" and ", $where);
        }
        
        $sql .= " order by sequencia, titulo ";
        
        $res = _query($sql, $bind);
        
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
        }
        
        return count($dados) > 0 ? $dados : null;
        
    }
    
}