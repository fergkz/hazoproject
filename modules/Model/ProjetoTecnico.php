<?php

namespace Model;

class ProjetoTecnico extends \MVC\Model{
    
    protected $ID;
    protected $projetoID;
    protected $projetoObj;
    protected $usuarioID;
    protected $usuarioObj;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setProjetoID( $projetoID ){
        $this->projetoID = $projetoID;
        return $this;
    }

    public function getProjetoID(){
        return $this->projetoID;
    }

    public function setProjetoObj( $projetoObj ){
        $this->projetoObj = $projetoObj;
        if( $this->projetoObj ){
            $this->projetoID = $this->projetoObj->getID();
        }
        return $this;
    }

    public function getProjetoObj(){
        if( !$this->projetoObj ){
            $this->projetoObj = Projeto::getByID($this->projetoID);
        }
        return $this->projetoObj;
    }

    public function setUsuarioID( $usuarioID ){
        $this->usuarioID = $usuarioID;
        return $this;
    }

    public function getUsuarioID(){
        return $this->usuarioID;
    }

    public function setUsuarioObj( $usuarioObj ){
        $this->usuarioObj = $usuarioObj;
        if( $this->usuarioObj ){
            $this->usuarioID = $this->usuarioObj->getID();
        }
        return $this;
    }

    public function getUsuarioObj(){
        if( !$this->usuarioObj ){
            $this->usuarioObj = Usuario::getByID($this->usuarioID);
        }
        return $this->usuarioObj;
    }
    
}