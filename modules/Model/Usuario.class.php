<?php

namespace Model;

use MVC\Session as Session;

class Usuario extends \MVC\Model{
    
    protected $ID;
    protected $login;
    protected $senha;
    protected $nome;
    protected $status;
    protected $email;
    protected $tipoUsuario;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setLogin( $login ){
        $this->login = $login;
        return $this;
    }

    public function getLogin(){
        return $this->login;
    }

    public function setSenha( $senha ){
        $this->senha = $senha;
        return $this;
    }

    public function getSenha(){
        return $this->senha;
    }

    public function setNome( $nome ){
        $this->nome = $nome;
        return $this;
    }

    public function getNome(){
        return $this->nome;
    }

    public function setStatus( $status ){
        $this->status = $status;
        return $this;
    }

    public function getStatus(){
        return $this->status;
    }

    public function setEmail( $email ){
        $this->email = $email;
        return $this;
    }

    public function getEmail(){
        return $this->email;
    }

    public function setTipoUsuario( $tipoUsuario ){
        $this->tipoUsuario = $tipoUsuario;
        return $this;
    }

    public function getTipoUsuario(){
        return $this->tipoUsuario;
    }
    
    public function login(){
        if( !$this->login ){
            return _setError('O login deve ser informado');
        }
        if( !$this->senha ){
            return _setError('A senha deve ser informada');
        }
        $sql = "select id from usuario where login = :login and password = :senha and status = 'A'";
        $bind['login'] = $this->login;
        $bind['senha'] = $this->senha;
        $res = _query($sql, $bind);
        if( !empty($res[0]['id']) ){
            $this->ID = $res[0]['id'];
            $this->load();
            self::logout();
            Session::set("transaction_user", $this);
            return $this;
        }else{
            return _setError("Usuário e senha inválidos");
        }
    }
    
    public static function logout(){
        return Session::destroy();
    }
    
    public static function online(){
        return Session::get("transaction_user");
    }
    
    public function getTipoInfo( $indice = null ){
        $dados['A'] = array(
            'descricao' => 'Administrador',
            'module'    => 'administrador'
        );
        $dados['G'] = array(
            'descricao' => 'Gerente',
            'module'    => 'gerente'
        );
        $dados['T'] = array(
            'descricao' => 'Técnico',
            'module'    => 'tecnico'
        );
        
        $tipo = $this->tipoUsuario;
        
        if( $indice ){
            return $dados[$tipo][$indice];
        }else{
            return $dados[$tipo];
        }
    }
    
    public static function lista( $tipo = null, $status = null ){
        
        $whereTmp = array();
        $where = array();
        $bind = array();
        
        if( $tipo ){
            if( is_array($tipo) ){
                $cont = 0;
                foreach( $tipo as $row ){
                    $cont++;
                    $whereTmp[] = "tipo_usuario = :tipo_usuario_".$cont;
                    $bind['tipo_usuario_'.$cont] = $row;
                }
                $where[] = " (".implode(" or ", $whereTmp).") ";
            }else{
                $where[] = "tipo_usuario = :tipo_usuario";
                $bind['tipo_usuario'] = $tipo;
            }
        }
        
        if( $status ){
            $where[] = "status = :status";
            $bind['status'] = $status;
        }
        
        $sql = "select id from usuario ";
        
        if( $where && count($where) > 0 ){
            $sql .= " where ".implode(" and ", $where);
        }
        
        $sql .= " order by field(status, 'A', 'I') ";
        
        $res = _query($sql, $bind);
        
        $dados = array();
        
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
        }
        
        return count($dados) > 0 ? $dados : null;
        
    }
    
}