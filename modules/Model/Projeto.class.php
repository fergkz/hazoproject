<?php

namespace Model;

class Projeto extends \MVC\Model{
    
    protected $ID;
    protected $titulo;
    protected $descricao;
    protected $status;
    protected $usuarioGerenteID;
    protected $usuarioGerenteObj;
    protected $clienteID;
    protected $clienteObj;
    
    private $statusInfo = array(
        'B' => array(
            'indice'    => 'B',
            'descricao' => 'Em Aberto'
        ),
        'A' => array(
            'indice'    => 'A',
            'descricao' => 'Ativo'
        ),
        'I' => array(
            'indice'    => 'I',
            'descricao' => 'Inativo'
        ),
        'D' => array(
            'indice'    => 'D',
            'descricao' => 'Em Desenvolvimento'
        )
    );
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setTitulo( $titulo ){
        $this->titulo = $titulo;
        return $this;
    }

    public function getTitulo(){
        return $this->titulo;
    }

    public function setDescricao( $descricao ){
        $this->descricao = $descricao;
        return $this;
    }

    public function getDescricao(){
        return $this->descricao;
    }

    public function setStatus( $status ){
        $this->status = $status;
        return $this;
    }

    public function getStatus(){
        return $this->status;
    }

    public function setUsuarioGerenteID( $usuarioGerenteID ){
        $this->usuarioGerenteID = $usuarioGerenteID;
        return $this;
    }

    public function getUsuarioGerenteID(){
        return $this->usuarioGerenteID;
    }

    public function setUsuarioGerenteObj( $usuarioGerenteObj ){
        $this->usuarioGerenteObj = $usuarioGerenteObj;
        if( $this->usuarioGerenteObj ){
            $this->usuarioGerenteID = $this->usuarioGerenteObj->getID();
        }
        return $this;
    }

    public function getUsuarioGerenteObj(){
        if( !$this->usuarioGerenteObj ){
            $this->usuarioGerenteObj = Usuario::getByID($this->usuarioGerenteID);
        }
        return $this->usuarioGerenteObj;
    }

    public function setClienteID( $clienteID ){
        $this->clienteID = $clienteID;
        return $this;
    }

    public function getClienteID(){
        return $this->clienteID;
    }

    public function setClienteObj( $clienteObj ){
        $this->clienteObj = $clienteObj;
        if( $this->clienteObj ){
            $this->clienteID = $this->clienteObj->getID();
        }
        return $this;
    }

    public function getClienteObj(){
        if( !$this->clienteObj ){
            $this->clienteObj = Cliente::getByID($this->clienteID);
        }
        return $this->clienteObj;
    }
    
    public function getStatusInfo( $modo = null ){
        if( $modo ){
            return $this->statusInfo[$this->status][$modo];
        }else{
            return $this->statusInfo[$this->status];
        }
    }
    
    public function getCronograma(){
        $sql = "select min(e.cronograma_id) as cronograma_id
                  from atividade a
                  join cronograma_atividade ca on ca.id = a.cronograma_atividade_id
                  join etapa e on e.id = ca.etapa_id
                 where a.projeto_id = :projeto_id";
        $bind['projeto_id'] = $this->ID;
        $res = _query($sql, $bind);
        if( !empty($res[0]['cronograma_id']) ){
            return Cronograma::getByID($res[0]['cronograma_id']);
        }else{
            return null;
        }
    }
    
    public function getAtividades( $status = null ){
        return Atividade::lista($this, $status);
    }
    
    public static function lista( $status = null, $gerente = null ){
        
        $bind = array();
        $where = array();
        $dados = array();
        
        if( $status ){
            if( is_array($status) ){
                if( count($status) > 0 ){
                    $cont = 0;
                    foreach( $status as $s ){
                        $cont++;
                        $nwhere[] = ":status{$cont}";
                        $bind["status{$cont}"] = $s;
                    }
                    $where[] = " status in (".implode(",", $nwhere).") ";
                }
            }else{
                $where[] = " status = :status ";
                $bind['status'] = $status;
            }
        }
        
        if( $gerente ){
            $gerenteID = is_object($gerente) ? $gerente->getID() : $gerente;
            $where[] = "usuario_gerente_id = :gerente";
            $bind['gerente'] = $gerenteID;
        }
        
        $sql = "select id from projeto";
        if( $where && count($where) > 0 ){
            $sql .= " where ".implode(" and ", $where);
        }
        
        $sql .= " order by field(status, 'A','I'), titulo ";
        
        $res = _query($sql, $bind);
        
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
        }
        
        return count($dados) > 0 ? $dados : null;
        
    }
    
    public function getDuracaoEstimada()
    {
        $sql = "  select coalesce(sum(atv.duracao_horas),0) as horas
                    from projeto prj
                    left join atividade atv on atv.projeto_id = prj.id
                   where prj.id = ?
                     and atv.status in ('A','O')  ";
        
        $bind[] = $this->ID;
        
        $res = _query($sql, $bind);
        
        return $res[0]['horas'];
    }
    
    public function getDuracaoExecutada()
    {
        $sql = "  select coalesce(sum(op.horas),0) as horas
                    from projeto prj
                    left join atividade atv on atv.projeto_id = prj.id
                    left join atividade_usuario_tecnico aut on aut.atividade_id = atv.id
                    left join atividade_operacao op on op.tecnico_atividade_id = aut.id
                   where prj.id = ?
                     and atv.status in ('A','O')  ";
        
        $bind[] = $this->ID;
        
        $res = _query($sql, $bind);
        
        return $res[0]['horas'];
    }
    
    public function getDadosRelatorio()
    {
        $sql = "
            select prj.titulo as prj_titulo,
                   prj.descricao as prj_descricao,
                   prj.status as prj_status_id,
                   prj.usuario_gerente_id,
                   u.name as usuario_gerente_name,
                   prj.cliente_id,
                   sum(coalesce(atv.duracao_horas, 0)) as duracao_estimada,
                   sum(coalesce(op.horas, 0)) as duracao_executada
              from projeto prj
              join usuario u on u.id = prj.usuario_gerente_id
              left join atividade atv on atv.projeto_id = prj.id and atv.status in ('A','O')
              left join atividade_usuario_tecnico aut on aut.atividade_id = atv.id
              left join atividade_operacao op on op.tecnico_atividade_id = aut.id

             where prj.id = ?

          group by prj.titulo,
                   prj.descricao,
                   prj.status,
                   prj.usuario_gerente_id,
                   u.name,
                   prj.cliente_id

        ";
        
        $bind[] = $this->ID;
        
        $res = _query($sql, $bind);
        
        debug($res);
        
        return $res[0];
    }
    
}