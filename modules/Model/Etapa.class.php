<?php

namespace Model;

class Etapa extends \MVC\Model{
    
    protected $ID;
    protected $sequencia;
    protected $descricao;
    protected $cronogramaID;
    protected $cronogramaObj;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }
    
    public function setSequencia( $sequencia ){
        $this->sequencia = $sequencia;
        return $this;
    }

    public function getSequencia(){
        return $this->sequencia;
    }

    public function setDescricao( $descricao ){
        $this->descricao = $descricao;
        return $this;
    }

    public function getDescricao(){
        return $this->descricao;
    }
    
    public function setCronogramaID( $cronogramaID ){
        $this->cronogramaID = $cronogramaID;
        return $this;
    }

    public function getCronogramaID(){
        return $this->cronogramaID;
    }

    public function setCronogramaObj( $cronogramaObj ){
        $this->cronogramaObj = $cronogramaObj;
        if( $this->cronogramaObj ){
            $this->cronogramaID = $this->cronogramaObj->getID();
        }
        return $this;
    }

    public function getCronogramaObj(){
        if( !$this->cronogramaObj ){
            $this->cronogramaObj = Cronograma::getByID($this->cronogramaID);
        }
        return $this->cronogramaObj;
    }
    
    public function getAtividades(){
        return EtapaAtividade::lista($this);
    }
    
    protected function triggerBeforeInsertUpdate(){
        if( !$this->new->sequencia ){
            $sql = "select coalesce(max(sequencia), 0) as sequencia
                      from etapa where cronograma_id = :cronograma_id";
            $bind['cronograma_id'] = $this->new->cronogramaID;
            $res = _query($sql, $bind);
            $this->new->sequencia = empty($res[0]['sequencia']) ? 1 : $res[0]['sequencia']+1;
        }
    }
    
    public static function lista( $cronograma ){
        $bind['cronograma_id'] = is_object($cronograma) ? $cronograma->getID() : $cronograma;
        $sql = "select id from etapa where cronograma_id = :cronograma_id order by sequencia";
        $res = _query($sql, $bind);
        $dados = array();
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
        }
        return count($dados) > 0 ? $dados : null;
    }
    
}