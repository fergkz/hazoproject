<?php

namespace Model;

use Lib\Format as Format;

class AtividadeOperacao extends \MVC\Model{
    
    protected $ID;
    protected $horas;
    protected $parecer;
    protected $tecnicoAtividadeID;
    protected $tecnicoAtividadeObj;
    protected $periodoDataInicio;
    protected $periodoDataFinal;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setHoras( $horas ){
        $this->horas = $horas;
        return $this;
    }

    public function getHoras(){
        return $this->horas;
    }

    public function setParecer( $parecer ){
        $this->parecer = $parecer;
        return $this;
    }

    public function getParecer(){
        return $this->parecer;
    }

    public function setTecnicoAtividadeID( $tecnicoAtividadeID ){
        $this->tecnicoAtividadeID = $tecnicoAtividadeID;
        return $this;
    }

    public function getTecnicoAtividadeID(){
        return $this->tecnicoAtividadeID;
    }

    public function setTecnicoAtividadeObj( $tecnicoAtividadeObj ){
        $this->tecnicoAtividadeObj = $tecnicoAtividadeObj;
        if( $this->tecnicoAtividadeObj ){
            $this->tecnicoAtividadeID = $this->tecnicoAtividadeObj->getID();
        }
        return $this;
    }

    public function getTecnicoAtividadeObj(){
        if( !$this->tecnicoAtividadeObj ){
            $this->tecnicoAtividadeObj = AtividadeUsuarioTecnico::getByID($this->tecnicoAtividadeID);
        }
        return $this->tecnicoAtividadeObj;
    }

    public function setPeriodoDataInicio( $periodoDataInicio ){
        $this->periodoDataInicio = Format::dataToDb($periodoDataInicio);
        return $this;
    }

    public function getPeriodoDataInicio(){
        return Format::dataToBr($this->periodoDataInicio);
    }

    public function setPeriodoDataFinal( $periodoDataFinal ){
        $this->periodoDataFinal = Format::dataToDb($periodoDataFinal);
        return $this;
    }

    public function getPeriodoDataFinal(){
        return Format::dataToBr($this->periodoDataFinal);
    }
    
    protected function triggerBeforeInsertUpdate()
    {
        if( !$this->getTecnicoAtividadeID() ){
            _raise("O técnico responsável deve ser informado");
        }
        if( !$this->horas ){
            _raise("Informe a quantidade de horas trabalhadas");
        }
        
        
        if( !$this->periodoDataInicio ){
            _raise("A data de início deve ser informada");
        }elseif( !\Lib\Validate::isDate($this->periodoDataInicio) ){
            _raise("A data de início está incorreta");
        }
        
        if( !$this->periodoDataFinal ){
            _raise("A data de término deve ser informada");
        }elseif( !\Lib\Validate::isDate($this->periodoDataFinal) ){
            _raise("A data de termino está incorreta");
        }
        
        if( Format::subDatas($this->periodoDataFinal, $this->periodoDataInicio) < 0 ){
            _raise("A data final não pode ser inferior á data inicial");
        }
        
        $tmpIni = explode("-", $this->periodoDataInicio);
        $tmpFim = explode("-", $this->periodoDataFinal);
        if( $tmpIni[1] !== $tmpFim[1] ){
            _raise("O período de execução das horas deve estar dentro do mesmo mês");
        }
        
    }
    
    public static function listaByAtividade( $atividade ){
        $bind[] = is_object($atividade) ? $atividade->getID() : $atividade;
        $sql = "select o.id
                  from atividade_operacao o
                  join atividade_usuario_tecnico at on at.id = o.tecnico_atividade_id
                 where at.atividade_id = ?
                 order by periodo_ini asc";
        $res = _query($sql, $bind);
        $dados = null;
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
        }
        return $dados;
    }
    
}