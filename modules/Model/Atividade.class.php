<?php

namespace Model;

use Model\EtapaAtividade as CronogramaAtividade;
use Lib\Format as Format;

class Atividade extends \MVC\Model
{
    protected $ID;
    protected $projetoID;
    protected $projetoObj;
    protected $dataPrevisaoInicio;
    protected $dataPrevisaoTermino;
    protected $duracaoHoras;
    protected $cronogramaAtividadeID;
    protected $cronogramaAtividadeObj;
    protected $status;
    
    public $statusDescricao = array(
        'A' => 'Ativa',
        'I' => 'Inativa',
        'O' => 'Concluída',
        'C' => 'Cancelada'
    );

    public function setID( $ID )
    {
        $this->ID = $ID;
        return $this;
    }

    public function getID()
    {
        return $this->ID;
    }

    public function setProjetoID( $projetoID )
    {
        $this->projetoID = $projetoID;
        return $this;
    }

    public function getProjetoID()
    {
        return $this->projetoID;
    }

    public function setProjetoObj( $projetoObj )
    {
        $this->projetoObj = $projetoObj;
        if( $this->projetoObj ){
            $this->projetoID = $this->projetoObj->getID();
        }
        return $this;
    }

    public function getProjetoObj()
    {
        if( !$this->projetoObj ){
            $this->projetoObj = Projeto::getByID($this->projetoID);
        }
        return $this->projetoObj;
    }

    public function setDataPrevisaoInicio( $dataPrevisaoInicio )
    {
        $this->dataPrevisaoInicio = Format::dataToDb($dataPrevisaoInicio);
        return $this;
    }

    public function getDataPrevisaoInicio()
    {
        return Format::dataToBr($this->dataPrevisaoInicio);
    }

    public function setDataPrevisaoTermino( $dataPrevisaoTermino )
    {
        $this->dataPrevisaoTermino = Format::dataToDb($dataPrevisaoTermino);
        return $this;
    }

    public function getDataPrevisaoTermino()
    {
        return Format::dataToBr($this->dataPrevisaoTermino);
    }

    public function setDuracaoHoras( $duracaoHoras )
    {
        $this->duracaoHoras = $duracaoHoras;
        return $this;
    }

    public function getDuracaoHoras()
    {
        return $this->duracaoHoras;
    }

    public function setCronogramaAtividadeID( $cronogramaAtividadeID )
    {
        $this->cronogramaAtividadeID = $cronogramaAtividadeID;
        return $this;
    }

    public function getCronogramaAtividadeID()
    {
        return $this->cronogramaAtividadeID;
    }

    public function setCronogramaAtividadeObj( $cronogramaAtividadeObj )
    {
        $this->cronogramaAtividadeObj = $cronogramaAtividadeObj;
        if( $this->cronogramaAtividadeObj ){
            $this->cronogramaAtividadeID = $this->cronogramaAtividadeObj->getID();
        }
        return $this;
    }

    public function getCronogramaAtividadeObj()
    {
        if( !$this->cronogramaAtividadeObj ){
            $this->cronogramaAtividadeObj = CronogramaAtividade::getByID($this->cronogramaAtividadeID);
        }
        return $this->cronogramaAtividadeObj;
    }

    public function setStatus( $status )
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus( $descricao = false )
    {
        if( $descricao ){
            return $this->statusDescricao[$this->status];
        }
        return $this->status;
    }

    public function listaOperacoes()
    {
        return AtividadeOperacao::listaByAtividade($this);
    }
    
    public function getMenorDataOperacao()
    {
        $sql = "select coalesce(min(op.periodo_ini), 0) as data
                  from atividade_operacao op
                  join atividade_usuario_tecnico aut on aut.id = op.tecnico_atividade_id
                  join atividade atv on atv.id = aut.atividade_id
                 where aut.atividade_id = ? and atv.status in ('A','O')
                  ";
        $bind[] = $this->ID;
        
        $res = _query($sql, $bind);
        
        return Format::dataToBr(@$res[0]['data']);
    }
    
    public function getMaiorDataOperacao()
    {
        $sql = "select coalesce(max(op.periodo_fim), 0) as data
                  from atividade_operacao op
                  join atividade_usuario_tecnico aut on aut.id = op.tecnico_atividade_id
                  join atividade atv on atv.id = aut.atividade_id
                 where aut.atividade_id = ? and atv.status in ('A','O')
                  ";
        $bind[] = $this->ID;
        
        $res = _query($sql, $bind);
        
        return Format::dataToBr(@$res[0]['data']);
    }
    
    public function triggerBeforeInsert()
    {
        $this->new->dataPrevisaoInicio = date('Y-m-d H:i:s');
        $this->new->dataPrevisaoTermino = date('Y-m-d H:i:s');
        $this->new->duracaoHoras = 0;
    }
    
    public function triggerBeforeInsertUpdate()
    {
        if( $this->updating && !in_array($this->new->status, array('I','C')) ){
            if( !$this->new->dataPrevisaoInicio ){
                _raise("A data de início deve ser informada");
            }elseif( !\Lib\Validate::isDate($this->new->dataPrevisaoInicio) ){
                _raise("A data de início está incorreta");
            }

            if( !$this->new->dataPrevisaoTermino ){
                _raise("A data de término deve ser informada");
            }elseif( !\Lib\Validate::isDate($this->new->dataPrevisaoTermino) ){
                _raise("A data de termino está incorreta");
            }
        }
    }

    public static function instanceByCronogramaAtividade( $cronogramaAtividade, $projeto )
    {
        $bind['cronograma_atividade_id'] = is_object($cronogramaAtividade) ? $cronogramaAtividade->getID() : $cronogramaAtividade;
        $bind['projeto_id'] = is_object($projeto) ? $projeto->getID() : $projeto;
        $sql = "select id from atividade where cronograma_atividade_id = :cronograma_atividade_id and projeto_id = :projeto_id";
        $res = _query($sql, $bind);
        if( !empty($res[0]['id']) ){
            return self::getByID($res[0]['id']);
        }else{
            return new Atividade();
        }
    }

    public function listaAtividadeUsuarioTecnico()
    {
        return AtividadeUsuarioTecnico::lista($this);
    }

    public static function lista( $projeto = null, $status = null )
    {
        $bind = $where = array( );
        if( $projeto ){
            $bind['projeto_id'] = is_object($projeto) ? $projeto->getID() : $projeto;
            $where[] = "projeto_id = :projeto_id";
        }

        if( $status ){
            if( is_array($status) ){
                $where[] = "status in ('".implode("','", $status)."') ";
            }else{
                $where[] = "status = :status";
                $bind['status'] = $status;
            }
        }
        
        $sql = "select id from atividade";

        if( $where && count($where) > 0 ){
            $sql .= " where ".implode(' and ', $where);
        }

        $res = _query($sql, $bind);

        $dados = array( );
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
        }

        return count($dados) > 0 ? $dados : null;
    }

    public static function listaByUsuario( $usuario )
    {
        $Usuario = is_object($usuario) ? $usuario : Usuario::getByID($usuario);
        
        if( !$Usuario ){
            return false;
        }
         
        $sql = "select a.id
                  from atividade a
                  join atividade_usuario_tecnico at on at.atividade_id = a.id
                  join projeto_tecnico pt on pt.id = at.projeto_tecnico_id
                 where pt.usuario_id = ?
                   and a.status = 'A'
                 order by a.data_prev_inicio";
        $bind[] = $Usuario->getID();
        
        $res = _query($sql, $bind);
        
        $dados = array();
        
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
        }
        
        return count($dados) > 0 ? $dados : false;
        
    }
    
}