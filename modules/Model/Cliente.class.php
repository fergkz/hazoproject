<?php

namespace Model;

class Cliente extends \MVC\Model{
    
    protected $ID;
    protected $nome;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setNome( $nome ){
        $this->nome = $nome;
        return $this;
    }

    public function getNome(){
        return $this->nome;
    }
    
    public static function lista( $status = null ){
        $bind = array();
        
        $sql = "select id from cliente ";
        
        if( $status !== null ){
            $bind['status'] = $status;
            $sql .= "where status = :status";
        }
        
        $res = _query($sql, $bind);
        
        $dados = array();
        
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
        }
        
        return count($dados) > 0 ? $dados : null;
        
    }
    
}