<?php

namespace Model;

class AtividadeUsuarioTecnico extends \MVC\Model{
    
    protected $ID;
    protected $atividadeID;
    protected $atividadeObj;
    protected $usuarioTecnicoID;
    protected $usuarioTecnicoObj;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setAtividadeID( $atividadeID ){
        $this->atividadeID = $atividadeID;
        return $this;
    }

    public function getAtividadeID(){
        return $this->atividadeID;
    }

    public function setAtividadeObj( $atividadeObj ){
        $this->atividadeObj = $atividadeObj;
        if( $this->atividadeObj ){
            $this->atividadeID = $this->atividadeObj->getID();
        }
        return $this;
    }

    public function getAtividadeObj(){
        if( !$this->atividadeObj ){
            $this->atividadeObj = Atividade::getByID($this->atividadeID);
        }
        return $this->atividadeObj;
    }

    public function setUsuarioTecnicoID( $usuarioTecnicoID ){
        $this->usuarioTecnicoID = $usuarioTecnicoID;
        return $this;
    }

    public function getUsuarioTecnicoID(){
        return $this->usuarioTecnicoID;
    }

    public function setUsuarioTecnicoObj( $usuarioTecnicoObj ){
        $this->usuarioTecnicoObj = $usuarioTecnicoObj;
        if( $this->usuarioTecnicoObj ){
            $this->usuarioTecnicoID = $this->usuarioTecnicoObj->getID();
        }
        return $this;
    }

    public function getUsuarioTecnicoObj(){
        if( !$this->usuarioTecnicoObj ){
            $this->usuarioTecnicoObj = ProjetoTecnico::getByID($this->usuarioTecnicoID);
        }
        return $this->usuarioTecnicoObj;
    }
    
    public static function lista( $atividade = null, $projetoTecnico = null ){
        $bind = $where = array();
        if( $atividade ){
            $bind['atividade_id'] = is_object($atividade) ? $atividade->getID() : $atividade;
            $where[] = "atividade_id = :atividade_id";
        }
        if( $projetoTecnico ){
            $bind['projeto_tecnico_id'] = is_object($projetoTecnico) ? $projetoTecnico->getID() : $projetoTecnico;
            $where[] = "projeto_tecnico_id = :projeto_tecnico_id";
        }
        
        $sql = "select id from atividade_usuario_tecnico";
        
        if( $where && count($where) > 0 ){
            $sql .= " where ".implode(' and ', $where);
        }
        
        $res = _query( $sql, $bind );
        
        $dados = array();
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
        }
        
        return count($dados) > 0 ? $dados : null;
    }
    
    public static function getOnlineAtividade( $atividade ){
        $atividadeID = is_object($atividade) ? $atividade->getID() : $atividade;
        $usuarioID = Usuario::online()->getID();
        $sql = "select aut.id 
                  from atividade_usuario_tecnico aut
                  join projeto_tecnico pt on pt.id = aut.projeto_tecnico_id
                 where atividade_id = ? and pt.usuario_id = ?";
        $bind = array( $atividadeID, $usuarioID );
        $res = _query($sql, $bind);
        foreach( $res as $row ){
            return self::getByID($row['id']);
        }
        return false;
    }
    
}