<?php

namespace Model;

class Cronograma extends \MVC\Model{
    
    protected $ID;
    protected $titulo;
    protected $status;
    
    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setTitulo( $titulo ){
        $this->titulo = $titulo;
        return $this;
    }

    public function getTitulo(){
        return $this->titulo;
    }

    public function setStatus( $status ){
        $this->status = $status;
        return $this;
    }

    public function getStatus(){
        return $this->status;
    }
    
    public function getEtapas(){
        return Etapa::lista($this);
    }
    
    public static function lista( $status = null ){
        $bind = array();
        
        $sql = "select id from cronograma";
        
        if( $status !== null ){
            $bind['status'] = $status;
            $sql .= " where status = :status ";
        }
        
        $res = _query($sql, $bind);
        
        $dados = array();
        
        foreach( $res as $row ){
            $dados[] = self::getByID($row['id']);
        }
        
        return count($dados) > 0 ? $dados : null;
        
    }
    
}