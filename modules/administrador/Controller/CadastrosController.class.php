<?php

use Model\Usuario as Usuario;
use Model\Cliente as Cliente;

class CadastrosController extends Controller\Administrador{

    public function administradoresAction(){
        
        $render['usuarios'] = Usuario::lista('A', null);
        $render['nome_modulo'] = 'Administradores Cadastrados';
        $render['lista_indice'] = 'Administrador';
        $render['lista_cadastro'] = 'cadastro-administrador';
        
        $this->view()->setTemplate('cadastros/lista.html')->display($render);
    }

    public function cadastroAdministradorAction( $usuarioID = null ){
        
        $Usuario = Usuario::getByID($usuarioID) ?: new Usuario();
        
        if( $_POST ){
            $Usuario->setLogin($_POST['login'])
                    ->setSenha($_POST['senha'])
                    ->setEmail($_POST['email'])
                    ->setNome($_POST['nome'])
                    ->setTipoUsuario('A')
                    ->setStatus(@$_POST['status']);
            
            if( $_POST['senha'] !== $_POST['rsenha'] ){
                _setError('As senhas informadas devem ser idênticas');
            }else{
                
                if( $Usuario->save() ){
                    _setSuccess("Cadastro salvo com sucesso");
                    $this->redirect(url."/administrador/cadastros/administradores");
                }
                
            }
        }
        
        $render['Usuario'] = $Usuario;
        $render['nome_modulo'] = 'Cadastro de Administrador';
        
        $this->view()->setTemplate('cadastros/edicao.html')->display($render);
    }

    public function tecnicosAction(){
        
        $render['usuarios'] = Usuario::lista('T', null);
        $render['nome_modulo'] = 'Técnicos Cadastrados';
        $render['lista_indice'] = 'Técnico';
        $render['lista_cadastro'] = 'cadastro-tecnico';
        
        $this->view()->setTemplate('cadastros/lista.html')->display($render);
    }

    public function cadastroTecnicoAction( $usuarioID = null ){
        
        $Usuario = Usuario::getByID($usuarioID) ?: new Usuario();
        
        if( $_POST ){
            $Usuario->setLogin($_POST['login'])
                    ->setSenha($_POST['senha'])
                    ->setEmail($_POST['email'])
                    ->setNome($_POST['nome'])
                    ->setTipoUsuario('T')
                    ->setStatus(@$_POST['status']);
            
            if( $_POST['senha'] !== $_POST['rsenha'] ){
                _setError('As senhas informadas devem ser idênticas');
            }else{
                
                if( $Usuario->save() ){
                    _setSuccess("Cadastro salvo com sucesso");
                    $this->redirect(url."/administrador/cadastros/tecnicos");
                }
                
            }
        }
        
        $render['Usuario'] = $Usuario;
        $render['nome_modulo'] = 'Cadastro de Técnico';
        
        $this->view()->setTemplate('cadastros/edicao.html')->display($render);
    }

    public function gerentesAction(){
        
        $render['usuarios'] = Usuario::lista('G', null);
        $render['nome_modulo'] = 'Gerentes Cadastrados';
        $render['lista_indice'] = 'Gerente';
        $render['lista_cadastro'] = 'cadastro-gerente';
        
        $this->view()->setTemplate('cadastros/lista.html')->display($render);
    }

    public function cadastroGerenteAction( $usuarioID = null ){
        
        $Usuario = Usuario::getByID($usuarioID) ?: new Usuario();
        
        if( $_POST ){
            $Usuario->setLogin($_POST['login'])
                    ->setSenha($_POST['senha'])
                    ->setEmail($_POST['email'])
                    ->setNome($_POST['nome'])
                    ->setTipoUsuario('G')
                    ->setStatus(@$_POST['status']);
            
            if( $_POST['senha'] !== $_POST['rsenha'] ){
                _setError('As senhas informadas devem ser idênticas');
            }else{
                
                if( $Usuario->save() ){
                    _setSuccess("Cadastro salvo com sucesso");
                    $this->redirect(url."/administrador/cadastros/gerentes");
                }
                
            }
        }
        
        $render['Usuario'] = $Usuario;
        $render['nome_modulo'] = 'Cadastro de Gerentes';
        
        $this->view()->setTemplate('cadastros/edicao.html')->display($render);
    }

    public function clientesAction(){
        
        $render['clientes'] = Cliente::lista(null);
        
        $this->view()->display($render);
    }

    public function cadastroClienteAction( $clienteID = null ){
        
        $Cliente = $clienteID ? Cliente::getByID($clienteID) : new Cliente();
        
        if( $_POST ){
            
            if( empty($_POST['excluir']) ){
                $Cliente->setNome($_POST['nome']);
                if( $Cliente->save() ){
                    _setSuccess("Cadastro salvo com sucesso");
                    $this->redirect(url."/administrador/cadastros/clientes");
                }
            }else{
                if( $Cliente->save('D') ){
                    $this->redirect(url."/administrador/cadastros/clientes");
                }
            }
                
        }
        
        $render['Cliente'] = $Cliente;
        
        $this->view()->display($render);
    }

}