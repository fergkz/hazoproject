<?php

use Model\Cronograma as Cronograma;
use Model\Etapa as Etapa;
use Model\EtapaAtividade as EtapaAtividade;

class CronogramaController extends Controller\Administrador{

    public function indexAction(){
        $render = array();
        
        $render['cronogramas'] = Cronograma::lista();
        
        $this->view()->display($render);
    }

    public function cadastroAction( $cronogramaID = null ){
        
        $render = array();
        $Cronograma = $cronogramaID ? Cronograma::getByID($cronogramaID) : new Cronograma();

        if( $_POST ){
            $Cronograma->setTitulo($_POST['titulo'])->setStatus($_POST['status']);

            $modo = $Cronograma->getID() ? "U" : "I";
            
            if( $Cronograma->save() ){
                
                if( !empty($_POST['seq_etapa']) && count($_POST['seq_etapa']) > 0 ){
                    foreach( $_POST['seq_etapa'] as $id => $seq ){
                        Etapa::getByID($id)->setSequencia($seq)->save();
                    }
                }
                
                if( !_getErrors() ){
                    if( $modo == "I" ){
                        _setSuccess("Cronograma criado com sucesso.");
                        $this->redirect(url."/administrador/cronograma/cadastro/".$Cronograma->getID());
                    }else{
                        _setSuccess("Cronograma alterado com sucesso com sucesso.");
                        $this->redirect(url."/administrador/cronograma");
                    }
                }
            }
        }
        
        $render['Cronograma'] = $Cronograma;
        $render['etapas'] = $Cronograma->getEtapas();
        
        $this->view()->display($render);
    }

    public function etapaCadastroAction( $cronogramaID = null, $etapaID = null ){
        $Cronograma = Cronograma::getByID($cronogramaID);
        if( !$Cronograma ){
            return 404;
        }
        
        $Etapa = $etapaID ? Etapa::getByID($etapaID) : new Etapa();
        
        if( $_POST ){
            
            $Etapa->setDescricao($_POST['titulo'])->setCronogramaID($cronogramaID);
            
            if( $Etapa->save() ){
                
                if( $_POST['atividade_nome'] ){
                    foreach( $_POST['atividade_nome'] as $ind => $nome ){
                        $nome = trim($nome);
                        
                        if( !empty($_POST['atividade_id'][$ind]) ){
                            $EtapaAtividade = EtapaAtividade::getByID($_POST['atividade_id'][$ind]);
                        }else{
                            $EtapaAtividade = new EtapaAtividade();
                        }
                        $EtapaAtividade->setEtapaID($Etapa->getID());
                        $EtapaAtividade->setSequencia($_POST['atividade_seq'][$ind]);
                        
                        if( $nome ){
                            $EtapaAtividade->setTitulo($nome);
                            $EtapaAtividade->save();
                        }elseif( $EtapaAtividade->getID() ){
                            $EtapaAtividade->save('D');
                        }
                        
                    }
                }
                
                if( !_getErrors() ){
                    $this->redirect(url."/administrador/cronograma/cadastro/{$cronogramaID}");
                }
                
            }
            
        }
        
        $render['Etapa'] = $Etapa;
        $render['atividades'] = $Etapa->getAtividades();
        $this->view()->display($render);
    }
    
}