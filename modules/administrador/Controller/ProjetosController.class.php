<?php

use Model\Projeto as Projeto;
use Model\Usuario as Usuario;
use Model\Cliente as Cliente;

class ProjetosController extends Controller\Administrador{

    public function indexAction(){
        $render['projetos'] = Projeto::listAll();
        $this->view()->display($render);
    }

    public function cadastroAction( $projetoID = null ){
        
        $Projeto = $projetoID ? Projeto::getByID($projetoID) : new Projeto();
        
        if( $_POST ){
            
            $Projeto->setTitulo($_POST['titulo'])
                    ->setDescricao($_POST['descricao'])
                    ->setStatus($_POST['status'])
                    ->setUsuarioGerenteID($_POST['gerente'])
                    ->setClienteID($_POST['cliente']);
                
            if( $Projeto->save() ){
                _setSuccess("Cadastro salvo com sucesso");
                $this->redirect(url."/administrador/projetos");
            }

        }
        
        $render['Projeto'] = $Projeto;
        
        $gerentes = Usuario::lista('G', 'A');
        if( $gerentes ){
            foreach( $gerentes as $Usuario ){
                $render['gerentes'][$Usuario->getID()] = $Usuario->getNome();
            }
        }
        
        $clientes = Cliente::lista();
        if( $clientes ){
            foreach( $clientes as $Usuario ){
                $render['clientes'][$Usuario->getID()] = $Usuario->getNome();
            }
        }
        
        $this->view()->display($render);
    }
    
}