<?php

use Model\Projeto as Projeto;

class RelatorioController extends Controller\Gerente
{

    /**
     * @return View
     */
    public function projetoAction( $id = null )
    {
        $Projeto = Projeto::getByID($id);
        
        if( !$Projeto ){
            return 404;
        }
        
        $render['Projeto'] = $Projeto;
        $render['atividades'] = $Projeto->getAtividades( array('A','O') );
        
        $this->view()->setTemplate('relatorio/projeto.twig')->display($render);
    }
    
}