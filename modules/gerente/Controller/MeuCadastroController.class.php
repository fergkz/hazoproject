<?php

use Model\Usuario as Usuario;

class MeuCadastroController extends Controller\Gerente
{

    public function indexAction()
    {

        $Usuario = Usuario::online();

        if( $_POST ){
            $Usuario->setLogin($_POST['login'])->setSenha($_POST['senha'])->setEmail($_POST['email']);

            if( empty($_POST['login']) || !$_POST['login'] ){
                _setError('O login deve ser informado');
            }elseif( strlen($_POST['login']) < 5 ){
                _setError('O login deve ter no mínimo 5 caracteres');
            }elseif( !$_POST['senha'] ){
                _setError('A senha deve ser informada');
            }elseif( $_POST['senha'] !== $_POST['rsenha'] ){
                _setError('As senhas informadas devem ser idênticas');
            }else{

                if( $Usuario->save() ){
                    $Usuario->login();
                    _setSuccess("Cadastro alterado com sucesso");
                    $this->redirect(url."/".$Usuario->getTipoInfo('module'));
                }
            }
        }

        $render['Usuario'] = $Usuario;

        $this->view()->display($render);
    }

}