<?php

use Model\Projeto as Projeto;
use Model\Cronograma as Cronograma;
use Model\Atividade as Atividade;
use Model\ProjetoTecnico as ProjetoTecnico;
use Model\Usuario as Usuario;
use Model\AtividadeUsuarioTecnico as AtividadeUsuarioTecnico;

class ProjetosController extends Controller\Gerente
{

    public function indexAction()
    {
        $render = array();

        $render['projetos'] = Projeto::lista(array( 'A', 'I' ), Usuario::online()->getID());

        $this->view()->display($render);
    }

    public function cadastroAction( $projetoID = null )
    {
        $render = array();

        $Projeto = Projeto::getByID($projetoID);


        if( !$Projeto /* || $Projeto->getStatus() !== 'A' */ ){
            return 404;
        }

        if( $_POST ){

            if( !empty($_POST['cronograma']) ){
                $Cronograma = Cronograma::getByID($_POST['cronograma']);
                $render['etapas'] = $Cronograma->getEtapas();

                if( $render['etapas'] ){
                    foreach( $render['etapas'] as $Etapa ){

                        $atividades = $Etapa->getAtividades();

                        if( $atividades ){
                            foreach( $atividades as $CronogramaAtividade ){
                                $Atividade = Atividade::instanceByCronogramaAtividade($CronogramaAtividade, $Projeto);
                                $Atividade->setProjetoID($projetoID)->setCronogramaAtividadeObj($CronogramaAtividade);
                                $Atividade->save();
                            }
                        }
                    }
                }
            }
        }elseif( $Projeto->getCronograma() ){

            $render['etapas'] = $Projeto->getCronograma()->getEtapas();
        }

        $render['Projeto'] = $Projeto;
        $cronogramas = Cronograma::lista('A');
        if( $cronogramas ){
            foreach( $cronogramas as $Cronograma ){
                $render['cronogramas'][$Cronograma->getID()] = $Cronograma->getTitulo();
            }
        }

        $atividadesProjeto = $Projeto->getAtividades();
        if( $atividadesProjeto ){
            foreach( $atividadesProjeto as $AtividadeProjeto ){
                if( $AtividadeProjeto ){
                    $render['atividades_projeto'][$AtividadeProjeto->getCronogramaAtividadeID()] = $AtividadeProjeto;
                }
            }
        }

        $this->view()->display($render);
    }

    public function cadastroAtividadeAction( $atividadeID = null, $acao = null, $atividadeTecnicoID = null )
    {
        $render = array( );

        $Atividade = Atividade::getByID($atividadeID);

        if( !$Atividade ){
            return 404;
        }

        $Projeto = $Atividade->getProjetoObj();

        if( $acao ){
            if( $acao === 'excluir-tecnico' && $atividadeTecnicoID ){
                $AtividadeTecnico = AtividadeUsuarioTecnico::getByID($atividadeTecnicoID);
                if( !$AtividadeTecnico ){
                    return 404;
                }
                if( $AtividadeTecnico->save('D') ){
                    _setSuccess("Técnico excluído com sucesso desta atividade");
                }
                $this->redirect(url.'/gerente/projetos/cadastro-atividade/'.$atividadeID);
            }
        }

        if( $_POST ){

            if( $_POST && $_POST['add_tecnico'] ){
                foreach( $_POST['add_tecnico'] as $projetoTecnicoID ){

                    if( !$projetoTecnicoID ){
                        continue;
                    }

                    $tecnicosAtividade = AtividadeUsuarioTecnico::lista($Atividade, $projetoTecnicoID);

                    if( !$tecnicosAtividade ){
                        $TecnicoAtividade = new AtividadeUsuarioTecnico();
                        $TecnicoAtividade->setAtividadeObj($Atividade);
                        $TecnicoAtividade->setUsuarioTecnicoID($projetoTecnicoID);
                        $TecnicoAtividade->save();
                    }
                }
            }

            $Atividade->setDataPrevisaoInicio($_POST['dt_ini'])
                    ->setDataPrevisaoTermino($_POST['dt_fim'])
                    ->setDuracaoHoras($_POST['horas'])
                    ->setStatus($_POST['status']);

            if( $Atividade->save() ){
                $this->redirect(url."/gerente/projetos/cadastro/".$Atividade->getProjetoID());
            }
        }

        $render['Projeto'] = $Atividade->getProjetoObj();
        $render['Atividade'] = $Atividade;

        $tecnicos = ProjetoTecnico::lista($Projeto);

        if( $tecnicos ){
            foreach( $tecnicos as $ProjetoTecnico ){
                $render['tecnicos_equipe'][$ProjetoTecnico->getID()] = $ProjetoTecnico->getUsuarioObj()->getNome();
            }
        }

        $render['tecnicos_atividade'] = AtividadeUsuarioTecnico::lista($Atividade);

        $this->view()->display($render);
    }

    public function cadastroNovaAtividadeAction( $atividadeCronogramaID = null, $projeto = null )
    {

        $render = array( );

        $AtividadeCronograma = \Model\EtapaAtividade::getByID($atividadeCronogramaID);

        if( !$AtividadeCronograma ){
            return 404;
        }

        $Projeto = Projeto::getByID($projeto);
        if( !$Projeto ){
            return 404;
        }

        $Atividade = Atividade::instanceByCronogramaAtividade($atividadeCronogramaID, $Projeto);
        $Atividade->setProjetoID($Projeto->getID())->setCronogramaAtividadeID($atividadeCronogramaID);
        $Atividade->save();
        $this->redirect(url.'/gerente/projetos/cadastro-atividade/'.$Atividade->getID());
    }

    public function cadastroEquipeAction( $projetoID = null, $acao = null, $projetoTecnicoID = null )
    {

        $render = array( );

        $Projeto = Projeto::getByID($projetoID);

        if( !$Projeto /* || $Projeto->getStatus() !== 'A' */ ){
            return 404;
        }

        if( $acao ){
            if( $acao === 'excluir-tecnico' && $projetoTecnicoID ){
                $ProjetoTecnico = ProjetoTecnico::getByID($projetoTecnicoID);
                if( !$ProjetoTecnico ){
                    return 404;
                }
                if( $ProjetoTecnico->save('D') ){
                    _setSuccess("Técnico excluído com sucesso desta equipe");
                }
                $this->redirect(url.'/gerente/projetos/cadastro-equipe/'.$projetoID);
            }
        }

        if( $_POST && $_POST['add_tecnico'] ){


            foreach( $_POST['add_tecnico'] as $usuarioID ){

                if( !$usuarioID ){
                    continue;
                }

                $tecnicosProjeto = ProjetoTecnico::lista($Projeto, $usuarioID);

                if( !$tecnicosProjeto ){
                    $ProjetoTecnico = new ProjetoTecnico();
                    $ProjetoTecnico->setProjetoObj($Projeto);
                    $ProjetoTecnico->setUsuarioID($usuarioID);
                    $ProjetoTecnico->save();
                }
            }

            if( !_getErrors() ){
                _setSuccess("Dados salvos com sucesso!");
            }
        }

        $render['Projeto'] = $Projeto;
        $tecnicos = Usuario::lista(array( 'T', 'G' ), 'A');

        if( $tecnicos ){
            foreach( $tecnicos as $Usuario ){
                $render['tecnicos_disponiveis'][$Usuario->getID()] = $Usuario->getNome();
            }
        }

        $render['tecnicos_projeto'] = ProjetoTecnico::lista($Projeto);

        $this->view()->display($render);
    }

    /**
     * @return Json
     */
    public function alteraStatusAtividadeAction( $id = null )
    {
        $render = array();
        
        $Atividade = Atividade::getByID($id);
        
        if( !$Atividade ){
            $this->error('Atividade inválida')->json();
        }
        
        $status = $Atividade->getStatus();
        
//        $Atividade->setDataPrevisaoInicio(null);
//        $Atividade->setDataPrevisaoTermino(null);
//        $Atividade->setDuracaoHoras(null);
        
        switch( $status ){
            case 'A':
                $Atividade->setStatus('I');
                break;
            case 'I':
                $Atividade->setStatus('A');
                break;
        }
        
        if( $Atividade->save() ){
            $render['status'] = true;
        }
        
        $this->json($render, true);
        
    }
    
}