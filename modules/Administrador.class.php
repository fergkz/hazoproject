<?php

namespace Controller;

use Model\Usuario as Usuario;

class Administrador extends \MVC\Controller{

    public function __construct(){
        
        $Usuario = Usuario::online();
        
        if( !$Usuario ){
            $this->redirect(url);
        }elseif( $Usuario->getTipoUsuario() !== 'A' ){
            _includeError(404);
            exit;
        }
        
    }

}