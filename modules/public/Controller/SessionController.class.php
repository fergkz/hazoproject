<?php

use Model\Usuario as Usuario;

class SessionController extends MVC\Controller{
    
    public function loginAction(){
        $render = array();
        if( $_POST ){
            $Usuario = new Usuario();
            $Usuario->setLogin($_POST['login'])->setSenha($_POST['senha']);
            if( $Usuario->login() ){
                $this->redirect(url."/".$Usuario->getTipoInfo('module'));
            }else{
                $render['erros'] = implode("<br/>", _getErrors());
                _clearErrors();
            }
        }
        $this->view()->display($render);
    }
    
    public function logoutAction(){
        Usuario::logout();
        $this->redirect(url);
    }
    
}