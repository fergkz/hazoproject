<?php

use Core\Model\User as User;

class IndexController extends MVC\Controller{

    public function indexAction(){
        if( User::online() ){
            $this->redirect(url."/".User::online()->getTipoInfo('module'));
        }else{
            $this->redirect(url."/session/login");
        }
    }

}