<?php

use Model\Usuario as Usuario;
use Model\Atividade as Atividade;
use Model\AtividadeOperacao as Operacao;
use Model\AtividadeUsuarioTecnico as TecnicoAtividade;
use Lib\Format as Format;

class AtividadesController extends Controller\Tecnico
{

    /**
     * Lista de atividades disponíveis para o usuário
     */
    public function indexAction()
    {
        $render['atividades'] = Atividade::listaByUsuario(Usuario::online());

        $view = $this->view();

        function getDataStatus( $data )
        {
            return Format::subDatas($data, date('d/m/Y'), 'd');
        }

        $view->declareFunction("getDataStatus");

        $view->display($render);
    }
    
    /**
     * Formulário de cadastro de horas pelo técnico
     * @param int $atividadeID - PK da atividade
     */
    public function cadastrarHorasAction( $atividadeID = null )
    {
        $Atividade = Atividade::getByID($atividadeID);
        if( !$Atividade ){
            return 404;
        }
        
        $render['Projeto'] = $Atividade->getProjetoObj();
        $render['Atividade'] = $Atividade;
        
        $this->view()->display($render);
    }

    /**
     * 
     * @param int $atividadeID - PK da atividade
     * @return ajax
     */
    public function gravarHorasAction( $atividadeID = null )
    {
        $Atividade = Atividade::getByID($atividadeID);
        if( !$Atividade ){
            return 404;
        }
        
        $Operacao = new Operacao();
        $Operacao->setHoras( $_POST['horas'] );
        $Operacao->setParecer( $_POST['parecer'] );
        $Operacao->setPeriodoDataInicio( $_POST['dt_inicio'] );
        $Operacao->setPeriodoDataFinal( $_POST['dt_fim'] );
        $Operacao->setTecnicoAtividadeObj( TecnicoAtividade::getOnlineAtividade($atividadeID) );
        
        if( !$Operacao->save() ){
            $mensagem = implode("<br/>", _getErrors());
            _clearErrors();
            $this->json(array(
                'status'  => false,
                'message' => $mensagem
            ));
        }else{
            _setSuccess('Horas informadas com sucesso');
            $this->json(array(
                'status'  => true,
                'message' => 'Operação gravada com sucesso'
            ));
        }
        
    }

}