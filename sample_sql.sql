-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.32 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2013-12-12 18:36:57
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for crepz631_hazo_project
DROP DATABASE IF EXISTS `crepz631_hazo_project`;
CREATE DATABASE IF NOT EXISTS `crepz631_hazo_project` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `crepz631_hazo_project`;


-- Dumping structure for table crepz631_hazo_project.anexo
DROP TABLE IF EXISTS `anexo`;
CREATE TABLE IF NOT EXISTS `anexo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `atividade_id` int(10) DEFAULT NULL,
  `demanda_id` int(10) DEFAULT NULL,
  `atividade_operacao_id` int(10) DEFAULT NULL,
  `file_name` varchar(250) NOT NULL,
  `file_type` varchar(50) NOT NULL,
  `file_size` varchar(50) NOT NULL,
  `file_content` longblob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_proj_anexo_proj_atividade` (`atividade_id`),
  KEY `FK_proj_anexo_proj_demanda` (`demanda_id`),
  KEY `FK_proj_anexo_proj_atividade_operacao` (`atividade_operacao_id`),
  CONSTRAINT `FK_proj_anexo_proj_atividade` FOREIGN KEY (`atividade_id`) REFERENCES `atividade` (`id`),
  CONSTRAINT `FK_proj_anexo_proj_atividade_operacao` FOREIGN KEY (`atividade_operacao_id`) REFERENCES `atividade_operacao` (`id`),
  CONSTRAINT `FK_proj_anexo_proj_demanda` FOREIGN KEY (`demanda_id`) REFERENCES `projeto` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crepz631_hazo_project.anexo: ~0 rows (approximately)
/*!40000 ALTER TABLE `anexo` DISABLE KEYS */;
/*!40000 ALTER TABLE `anexo` ENABLE KEYS */;


-- Dumping structure for table crepz631_hazo_project.atividade
DROP TABLE IF EXISTS `atividade`;
CREATE TABLE IF NOT EXISTS `atividade` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `projeto_id` int(10) DEFAULT NULL,
  `data_prev_inicio` date DEFAULT NULL,
  `data_prev_termino` date DEFAULT NULL,
  `duracao_horas` int(11) DEFAULT '0',
  `cronograma_atividade_id` int(10) NOT NULL,
  `status` char(1) DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `FK_proj_atividade_proj_demanda` (`projeto_id`),
  KEY `fk_atividade_cronograma_atividade1_idx` (`cronograma_atividade_id`),
  CONSTRAINT `fk_atividade_cronograma_atividade1` FOREIGN KEY (`cronograma_atividade_id`) REFERENCES `cronograma_atividade` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_proj_atividade_proj_demanda` FOREIGN KEY (`projeto_id`) REFERENCES `projeto` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

-- Dumping data for table crepz631_hazo_project.atividade: ~44 rows (approximately)
/*!40000 ALTER TABLE `atividade` DISABLE KEYS */;
/*!40000 ALTER TABLE `atividade` ENABLE KEYS */;


-- Dumping structure for table crepz631_hazo_project.atividade_operacao
DROP TABLE IF EXISTS `atividade_operacao`;
CREATE TABLE IF NOT EXISTS `atividade_operacao` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `horas` int(11) NOT NULL DEFAULT '0',
  `parecer` longtext,
  `tecnico_atividade_id` int(10) NOT NULL,
  `periodo_ini` date NOT NULL,
  `periodo_fim` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_atividade_operacao_atividade_usuario_tecnico1_idx` (`tecnico_atividade_id`),
  CONSTRAINT `fk_atividade_operacao_atividade_usuario_tecnico1` FOREIGN KEY (`tecnico_atividade_id`) REFERENCES `atividade_usuario_tecnico` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table crepz631_hazo_project.atividade_operacao: ~2 rows (approximately)
/*!40000 ALTER TABLE `atividade_operacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `atividade_operacao` ENABLE KEYS */;


-- Dumping structure for table crepz631_hazo_project.atividade_usuario_tecnico
DROP TABLE IF EXISTS `atividade_usuario_tecnico`;
CREATE TABLE IF NOT EXISTS `atividade_usuario_tecnico` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `atividade_id` int(10) NOT NULL,
  `projeto_tecnico_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_proj_atividade_grupo_proj_atividade` (`atividade_id`),
  KEY `fk_atividade_usuario_tecnico_projeto_equipe1_idx` (`projeto_tecnico_id`),
  CONSTRAINT `fk_atividade_usuario_tecnico_projeto_equipe1` FOREIGN KEY (`projeto_tecnico_id`) REFERENCES `projeto_tecnico` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_proj_atividade_grupo_proj_atividade` FOREIGN KEY (`atividade_id`) REFERENCES `atividade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table crepz631_hazo_project.atividade_usuario_tecnico: ~1 rows (approximately)
/*!40000 ALTER TABLE `atividade_usuario_tecnico` DISABLE KEYS */;
/*!40000 ALTER TABLE `atividade_usuario_tecnico` ENABLE KEYS */;


-- Dumping structure for table crepz631_hazo_project.cliente
DROP TABLE IF EXISTS `cliente`;
CREATE TABLE IF NOT EXISTS `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='	';

-- Dumping data for table crepz631_hazo_project.cliente: ~1 rows (approximately)
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
REPLACE INTO `cliente` (`id`, `nome`) VALUES
	(5, 'Lorenza Construtora S.A.');
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;


-- Dumping structure for table crepz631_hazo_project.cronograma
DROP TABLE IF EXISTS `cronograma`;
CREATE TABLE IF NOT EXISTS `cronograma` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  UNIQUE KEY `titulo_UNIQUE` (`titulo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table crepz631_hazo_project.cronograma: ~1 rows (approximately)
/*!40000 ALTER TABLE `cronograma` DISABLE KEYS */;
REPLACE INTO `cronograma` (`id`, `titulo`, `status`) VALUES
	(3, 'PMBOK 2013', 'A');
/*!40000 ALTER TABLE `cronograma` ENABLE KEYS */;


-- Dumping structure for table crepz631_hazo_project.cronograma_atividade
DROP TABLE IF EXISTS `cronograma_atividade`;
CREATE TABLE IF NOT EXISTS `cronograma_atividade` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `sequencia` int(11) NOT NULL,
  `titulo` varchar(250) NOT NULL,
  `descricao` longtext,
  `etapa_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cronograma_atividade_atividade_etapa1_idx` (`etapa_id`),
  CONSTRAINT `fk_cronograma_atividade_atividade_etapa1` FOREIGN KEY (`etapa_id`) REFERENCES `etapa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

-- Dumping data for table crepz631_hazo_project.cronograma_atividade: ~44 rows (approximately)
/*!40000 ALTER TABLE `cronograma_atividade` DISABLE KEYS */;
REPLACE INTO `cronograma_atividade` (`id`, `sequencia`, `titulo`, `descricao`, `etapa_id`) VALUES
	(21, 1, 'Desenvolver o termo de abertura do projeto', NULL, 6),
	(22, 2, 'Desenvolver a declaração do escopo preliminar do projeto', NULL, 6),
	(23, 3, 'Desenvolver o plano de gerenciamento do projeto', NULL, 6),
	(24, 4, 'Orientar e gerenciar a execução projeto', NULL, 6),
	(25, 5, 'Monitorar e controlar o trabalho do projeto', NULL, 6),
	(26, 6, 'Controle integrado de mudanças', NULL, 6),
	(27, 7, 'Encerrar o projeto', NULL, 6),
	(28, 1, 'Planejamento do escopo', NULL, 7),
	(29, 2, 'Definição do escopo', NULL, 7),
	(30, 3, 'Criar a Estrutura Analítica de Processo (EAP)', NULL, 7),
	(31, 4, 'Verificação do escopo', NULL, 7),
	(32, 5, 'Controle do escopo', NULL, 7),
	(33, 1, 'Definição da atividade', NULL, 8),
	(34, 2, 'Sequenciamento de atividades', NULL, 8),
	(35, 3, 'Estimativa de recursos da atividade', NULL, 8),
	(36, 4, 'Estimativa de duração da atividade', NULL, 8),
	(37, 5, 'Desenvolvimento do cronograma', NULL, 8),
	(38, 6, 'Controle do cronograma', NULL, 8),
	(39, 1, 'Estimativa de custos', NULL, 9),
	(40, 2, 'Orçamentação', NULL, 9),
	(41, 3, 'Controle de custos', NULL, 9),
	(42, 1, 'Planejamento da qualidade', NULL, 10),
	(43, 2, 'Realizar a garantia da qualidade', NULL, 10),
	(44, 3, 'Realizar o controle da qualidade', NULL, 10),
	(45, 1, 'Planejamento de recursos humanos', NULL, 11),
	(46, 2, 'Contratar ou mobilizar a equipe do projeto', NULL, 11),
	(47, 3, 'Desenvolver a equipe de projeto', NULL, 11),
	(48, 4, 'Gerenciar a equipe de projeto', NULL, 11),
	(49, 1, 'Planejamento das comunicações', NULL, 12),
	(50, 2, 'Distribuição das informações', NULL, 12),
	(51, 3, 'Relatório de desempenho', NULL, 12),
	(52, 4, 'Gerenciar as partes interessadas', NULL, 12),
	(53, 1, 'Planejamento do gerenciamento de riscos', NULL, 13),
	(54, 2, 'Identificação de riscos', NULL, 13),
	(55, 3, 'Análise qualitativa de riscos', NULL, 13),
	(56, 4, 'Análise quantitativa de riscos', NULL, 13),
	(57, 5, 'Planejamento de respostas a riscos', NULL, 13),
	(58, 6, 'Monitoramento e controle de riscos', NULL, 13),
	(59, 1, 'Planejar compras e aquisições', NULL, 14),
	(60, 2, 'Planejar contratações', NULL, 14),
	(61, 3, 'Solicitar respostas de fornecedores', NULL, 14),
	(62, 4, 'Selecionar fornecedores', NULL, 14),
	(63, 5, 'Administração de contrato', NULL, 14),
	(64, 6, 'Encerramento de contrato', NULL, 14);
/*!40000 ALTER TABLE `cronograma_atividade` ENABLE KEYS */;


-- Dumping structure for table crepz631_hazo_project.etapa
DROP TABLE IF EXISTS `etapa`;
CREATE TABLE IF NOT EXISTS `etapa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sequencia` int(11) NOT NULL,
  `descricao` varchar(250) NOT NULL,
  `cronograma_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_etapa_cronograma1_idx` (`cronograma_id`),
  CONSTRAINT `fk_etapa_cronograma1` FOREIGN KEY (`cronograma_id`) REFERENCES `cronograma` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- Dumping data for table crepz631_hazo_project.etapa: ~9 rows (approximately)
/*!40000 ALTER TABLE `etapa` DISABLE KEYS */;
REPLACE INTO `etapa` (`id`, `sequencia`, `descricao`, `cronograma_id`) VALUES
	(6, 1, 'Gerenciamento de integração do projeto', 3),
	(7, 2, 'Gerenciamento do escopo do projeto', 3),
	(8, 3, 'Gerenciamento de tempo de projeto', 3),
	(9, 4, 'Gerenciamento de custos do projeto', 3),
	(10, 5, 'Gerenciamento da qualidade do projeto', 3),
	(11, 6, 'Gerenciamento de recursos humanos do projeto', 3),
	(12, 7, 'Gerenciamento das comunicações do projeto', 3),
	(13, 8, 'Gerenciamento de riscos do projeto', 3),
	(14, 9, 'Gerenciamento de aquisições do projeto', 3);
/*!40000 ALTER TABLE `etapa` ENABLE KEYS */;


-- Dumping structure for table crepz631_hazo_project.perfil_usuario
DROP TABLE IF EXISTS `perfil_usuario`;
CREATE TABLE IF NOT EXISTS `perfil_usuario` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `status` char(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table crepz631_hazo_project.perfil_usuario: ~0 rows (approximately)
/*!40000 ALTER TABLE `perfil_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `perfil_usuario` ENABLE KEYS */;


-- Dumping structure for table crepz631_hazo_project.projeto
DROP TABLE IF EXISTS `projeto`;
CREATE TABLE IF NOT EXISTS `projeto` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(250) NOT NULL,
  `descricao` longtext,
  `status` char(1) NOT NULL,
  `usuario_gerente_id` int(10) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_projeto_usuario1_idx` (`usuario_gerente_id`),
  KEY `fk_projeto_cliente1_idx` (`cliente_id`),
  CONSTRAINT `fk_projeto_cliente1` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_projeto_usuario1` FOREIGN KEY (`usuario_gerente_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table crepz631_hazo_project.projeto: ~1 rows (approximately)
/*!40000 ALTER TABLE `projeto` DISABLE KEYS */;
REPLACE INTO `projeto` (`id`, `titulo`, `descricao`, `status`, `usuario_gerente_id`, `cliente_id`) VALUES
	(5, 'Construção Estádio Piramboia', 'Elaboração do projeto de construção do estádio Piramboia da Av. Iguaçu em Curitiba.\r\n\r\nResponsável pela elaboração do projeto: Gerente 02\r\nSponsor do projeto: Carlos Marco Cardoso, associado da empresa Lorenza S.A.', 'A', 12, 5);
/*!40000 ALTER TABLE `projeto` ENABLE KEYS */;


-- Dumping structure for table crepz631_hazo_project.projeto_tecnico
DROP TABLE IF EXISTS `projeto_tecnico`;
CREATE TABLE IF NOT EXISTS `projeto_tecnico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projeto_id` int(10) NOT NULL,
  `usuario_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_projeto_equipe_projeto1_idx` (`projeto_id`),
  KEY `fk_projeto_equipe_usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_projeto_equipe_projeto1` FOREIGN KEY (`projeto_id`) REFERENCES `projeto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_projeto_equipe_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table crepz631_hazo_project.projeto_tecnico: ~1 rows (approximately)
/*!40000 ALTER TABLE `projeto_tecnico` DISABLE KEYS */;
/*!40000 ALTER TABLE `projeto_tecnico` ENABLE KEYS */;


-- Dumping structure for table crepz631_hazo_project.usuario
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(150) NOT NULL,
  `status` char(1) NOT NULL,
  `email` varchar(250) DEFAULT NULL,
  `tipo_usuario` char(1) NOT NULL COMMENT 'tipo_usuario:\n    [C]liente, [T]écnico\n    Nos casos de gerente de projeto, seleciona-se um técnico cadastrado em usuário e define na tabela "projeto"',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- Dumping data for table crepz631_hazo_project.usuario: ~11 rows (approximately)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
REPLACE INTO `usuario` (`id`, `login`, `password`, `name`, `status`, `email`, `tipo_usuario`) VALUES
	(10, 'admin', 'abcd', 'Administrador', 'A', '', 'A'),
	(11, 'gerente1', 'gerente', 'Gerente 01', 'A', '', 'G'),
	(12, 'gerente2', 'gerente', 'Gerente 02', 'A', '', 'G'),
	(14, 'gerente3', 'gerente', 'Gerente 03', 'A', '', 'G'),
	(15, 'tecnico1', 'tecnico', 'Técnico 01', 'A', '', 'T'),
	(16, 'tecnico2', 'tecnico', 'Técnico 02', 'A', '', 'T'),
	(17, 'tecnico3', 'tecnico', 'Técnico 03', 'A', '', 'T'),
	(18, 'tecnico4', 'tecnico', 'Técnico 04', 'A', '', 'T'),
	(19, 'tecnico5', 'tecnico', 'Técnico 05', 'A', '', 'T'),
	(21, 'tecnico6', 'tecnico', 'Técnico 06', 'A', '', 'T'),
	(22, 'tecnico7', 'tecnico', 'Técnico 07', 'A', '', 'T');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
