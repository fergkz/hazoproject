-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.5.27 - MySQL Community Server (GPL)
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              8.0.0.4396
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela crepz631_hazo_portal.hazo_group
DROP TABLE IF EXISTS `hazo_group`;
CREATE TABLE IF NOT EXISTS `hazo_group` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `mode` varchar(50) NOT NULL,
  `status` char(1) NOT NULL,
  `default_page` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.hazo_menu
DROP TABLE IF EXISTS `hazo_menu`;
CREATE TABLE IF NOT EXISTS `hazo_menu` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mode` varchar(50) DEFAULT NULL,
  `module` varchar(100) DEFAULT NULL,
  `submodule` varchar(100) DEFAULT NULL,
  `description` varchar(25) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL,
  `description_route` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_hazo_menu_hazo_group` (`group_id`),
  KEY `FK_hazo_menu_hazo_menu` (`parent`),
  CONSTRAINT `FK_hazo_menu_hazo_group` FOREIGN KEY (`group_id`) REFERENCES `hazo_group` (`id`),
  CONSTRAINT `FK_hazo_menu_hazo_menu` FOREIGN KEY (`parent`) REFERENCES `hazo_menu` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.hazo_permission
DROP TABLE IF EXISTS `hazo_permission`;
CREATE TABLE IF NOT EXISTS `hazo_permission` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mode` varchar(50) NOT NULL,
  `module` varchar(100) NOT NULL,
  `submodule` varchar(100) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_hazo_permission_hazo_group` (`group_id`),
  CONSTRAINT `FK_hazo_permission_hazo_group` FOREIGN KEY (`group_id`) REFERENCES `hazo_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.


-- Copiando estrutura para tabela crepz631_hazo_portal.hazo_user
DROP TABLE IF EXISTS `hazo_user`;
CREATE TABLE IF NOT EXISTS `hazo_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `login` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(150) NOT NULL,
  `status` char(1) NOT NULL,
  `email` varchar(250) DEFAULT NULL,
  `date_last_access` datetime DEFAULT NULL,
  `activation_code` varchar(2000) DEFAULT NULL,
  `image_name` varchar(200) DEFAULT NULL,
  `image_content` longblob,
  `image_size` varchar(200) DEFAULT NULL,
  `image_type` varchar(200) DEFAULT NULL,
  `image_filename` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_hazo_user_hazo_group` (`group_id`),
  CONSTRAINT `FK_hazo_user_hazo_group` FOREIGN KEY (`group_id`) REFERENCES `hazo_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Exportação de dados foi desmarcado.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
